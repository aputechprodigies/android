package com.atos.fishnet.fishnet;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RatingBar;

import androidx.fragment.app.Fragment;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.component.ApplicationComponent;
import com.atos.fishnet.di.component.DaggerApplicationComponent;
import com.atos.fishnet.di.module.ContextModule;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class FishnetApplication extends Application {

    private static Context mContext;
    ApplicationComponent mAppComponent;
    private ImageLoader mImageLoader;

    @Override
    public void onCreate() {
        super.onCreate();
        FishnetStorage.getInstance().init(this);
        initDaggerInjection();
        mContext = this;

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        System.out.println("Onesignal : "+result.notification.payload.notificationID);
                    }
                })
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    public static FishnetApplication getInstance(){
        return new FishnetApplication();
    }

    private void initDaggerInjection() {
        mAppComponent = DaggerApplicationComponent.builder().contextModule(new ContextModule(this)).build();
        mAppComponent.injectApplication(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static FishnetApplication get(Activity activity) {
        return (FishnetApplication) activity.getApplication();
    }

    public static FishnetApplication get(Fragment fragment) {
        Activity activity = fragment.getActivity();
        if (activity != null) {
            return (FishnetApplication) fragment.getActivity().getApplication();
        }
        return null;
    }

    public ApplicationComponent getAppComponent() {
        return mAppComponent;
    }
}


