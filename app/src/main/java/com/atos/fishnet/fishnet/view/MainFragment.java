package com.atos.fishnet.fishnet.view;

import androidx.fragment.app.Fragment;

public class MainFragment extends Fragment {
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden==false) {
            onStart();
        }
    }
}