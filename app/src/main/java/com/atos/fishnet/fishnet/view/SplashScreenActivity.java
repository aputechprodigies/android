package com.atos.fishnet.fishnet.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieDrawable;
import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.login.view.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.iv_image_icon)
    LottieAnimationView iv_image_icon;

    private static int SPLASH_TIME_OUT = 6000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);

        LottieDrawable drawable = new LottieDrawable();
        LottieComposition.Factory.fromAssetFileName(this, "fish_splash.json", (composition ->
        {
            drawable.setComposition(composition);
            drawable.playAnimation();
            iv_image_icon.setImageDrawable(drawable);

        }));

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

                new Handler().postDelayed(new Runnable() {
                    @Override

                    public void run() {

                        if ((FishnetStorage.getInstance().getAuthToken() != null && !FishnetStorage.getInstance().getAuthToken().isEmpty())
                                || FishnetStorage.getInstance().getIsSkipRegistration()) {

                            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));

                        } else {
                            // TODO: 2019-12-26 Update correct path if token retained
                            Intent homeIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                            startActivity(homeIntent);
                        }
                        finish();
                    }

                },SPLASH_TIME_OUT);
    }
}
