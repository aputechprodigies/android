package com.atos.fishnet.fishnet.view;

import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.atos.fishnet.R;
import com.atos.fishnet.eWallet.view.EWalletFragment;
import com.atos.fishnet.notification.view.NotificationFragment;
import com.atos.fishnet.home.view.HomeFragment;
import com.atos.fishnet.profile.view.ProfileFragment;

import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;

public class MainActivity extends BaseActivity {

    HomeFragment homeFragment = HomeFragment.getInstance();
    NotificationFragment notificationFragment = NotificationFragment.getInstance();
    ProfileFragment profileFragment = ProfileFragment.getInstance();
    EWalletFragment ewalletFragment = EWalletFragment.getInstance();

    SmoothBottomBar bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.bottomBar);
        initUserNavigation();
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateStatusBarColor("#357DF9");
    }

    private void initUserNavigation() {
       bottomNavigationView.setOnItemSelectedListener(new OnItemSelectedListener() {
           @Override
           public void onItemSelect(int i) {
               switch (i) {
                   case 0:

                       FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                               .replace(R.id.container,homeFragment)
                               .show(homeFragment);
                       transaction.commit();

                       break;
                   case 1:
                       FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction()
                               .replace(R.id.container, notificationFragment)
                               .show(notificationFragment);
                       transaction2.commit();
                       break;
                   case 2:
                       FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction()
                               .replace(R.id.container,ewalletFragment)
                               .show(ewalletFragment);
                       transaction3.commit();
                       break;
                   case 3:
                       getSafeFragmentManager().beginTransaction()
                               .replace(R.id.container,profileFragment)
                               .show(profileFragment)
                               .commit();
                       break;
               }
           }
       });

        // initNavigator();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .add(R.id.container,homeFragment)
                .show(homeFragment);
        transaction.commit();

    }
}
