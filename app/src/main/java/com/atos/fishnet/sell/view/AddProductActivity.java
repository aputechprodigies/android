package com.atos.fishnet.sell.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.atos.fishnet.fishnet.view.MainActivity;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.sell.presenter.ImageCapturePresenter;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.abhinay.input.CurrencyEditText;

import static me.abhinay.input.CurrencySymbols.MALAYSIA;

public class AddProductActivity extends BaseActivity implements ImageCapturePresenter.View {

    @BindView(R.id.img_capture)
    ImageView img_capture;
    @BindView(R.id.et_species_name)
    EditText et_species_name;
    @BindView(R.id.total_price)
    CurrencyEditText total_price;
    @BindView(R.id.et_weight)
    EditText et_weight;
    @BindView(R.id.btn_publish)
    Button btn_publish;

    private ImageCapturePresenter mPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);
        mPresenter = new ImageCapturePresenter(this,this);

        Glide
                .with(this)
                .load(Uri.parse(getIntent().getExtras().getString("uri")))
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.bar_curve_white)
                .into(img_capture);

        if(getIntent().getExtras().getString("name") !=null)
            et_species_name.setText(getIntent().getExtras().getString("name"));

        total_price.setCurrency(MALAYSIA);
        total_price.setDelimiter(false);
        total_price.setSpacing(false);
        total_price.setDecimals(true);

        btn_publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.createProduct(et_species_name.getText().toString(),total_price.getCleanDoubleValue(),et_weight.getText().toString(),getIntent().getExtras().getString("uri"));
            }
        });
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void onNext(String message) {
        finish();
        Toast.makeText(this, "Create Product: "+message, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AddProductActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void updateImageView(Uri uri) {

    }

    @Override
    public void onContinueAction(Root root) {

    }
}
