package com.atos.fishnet.sell.view;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.utils.ChoosePhoto;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.sell.presenter.ImageCapturePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SellProductActivity extends BaseActivity implements ImageCapturePresenter.View {

    private static final int CROP_PIC = 2022;
    private Intent intent;
    private boolean isStatus = false;
    @BindView(R.id.listing_container)
    LinearLayout listing_container;
    @BindView(R.id.progress_layout)
    RelativeLayout progress_layout;
    @BindView(R.id.img_back)
    ImageView img_back;

    private ImageCapturePresenter mPresenter;
    Handler mHandler = new Handler(Looper.getMainLooper());

    public SellProductActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_product);
        ButterKnife.bind(this);
        mPresenter = new ImageCapturePresenter(this, this);
        mPresenter.init(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listing_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onTakePhotoSquare(SellProductActivity.this);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        isStatus = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ChoosePhoto.SELECT_PICTURE_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // NOTE: Need to handle image uploading using folder
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (resultCode == CameraKitActivity.CAMERAKIT_RESULT) {
            Uri picUri = data.getData();
            mPresenter.handleCameraResult(picUri);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onError(String error) {

    }

    @Override
    public void showComplete() {
        if (isStatus)
            startActivity(intent);
        else
            Toast.makeText(this, "No fish detected", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onNext(String message) {

    }
    @Override
    public void showProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.VISIBLE));
    }

    @Override
    public void dismissProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.GONE));
    }

    @Override
    public void updateImageView(Uri uri) {
        mPresenter.recognizeFish(uri);
    }

    @Override
    public void onContinueAction(Root root) {
        isStatus = true;
        intent = new Intent(this, ImageCaptureActivity.class);
        intent.putExtra("uri", root.getLocation());
        intent.putExtra("message", root.getMessage());
        intent.putExtra("fish", root.getFish());
        intent.putExtra("index", root.getIndex());
    }
}
