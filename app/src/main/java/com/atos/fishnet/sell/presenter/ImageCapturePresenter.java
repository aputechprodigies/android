package com.atos.fishnet.sell.presenter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.fishnet.FishnetApplication;
import com.atos.fishnet.fishnet.utils.ChoosePhoto;
import com.atos.fishnet.fishnet.utils.FileUtil;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.sell.view.CameraKitActivity;
import com.atos.fishnet.sell.view.SellProductActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.atos.fishnet.fishnet.utils.FocusView.INIT_TYPE_PARCELABLE;
import static com.atos.fishnet.fishnet.utils.FocusView.TYPE_SQUARE;

public class ImageCapturePresenter {

    public static final int PICK_IMAGE = 3;

    private Uri mUrlPath;
    private View mView;
    private ChoosePhoto mChoosePhoto;
    private String config;
    private Context mContext;

    private APIInterface mApiInterface;
    private SellProductActivity sellProductActivity;


    public void init(Activity activity) {
        mChoosePhoto = new ChoosePhoto(activity);
    }

    public ImageCapturePresenter(View view, Context context) {
        this.mView = view;
        this.mContext = context;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    /**
     * Gets the content:// URI  from the given corresponding path to a file
     *
     * @param context
     * @param imageFile
     * @return content Uri
     */
    public static Uri getImageContentUri(Context context, java.io.File imageFile) {

        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return Uri.parse(imageFile.getAbsolutePath());
            }
        }

    }

    public void onTakePhotoSquare(Activity activity) {
        Intent intent = new Intent(activity, CameraKitActivity.class);
        intent.putExtra(INIT_TYPE_PARCELABLE, TYPE_SQUARE);

        activity.startActivityForResult(intent, CameraKitActivity.CAMERAKIT_RESULT);
    }

    public void handleCameraResult(Uri url) {
        String urlPath = FileUtil.getInstance(mContext).getRealPathFromURI(mContext,url);
        mUrlPath = Uri.parse(urlPath);
        mView.updateImageView(mUrlPath);
    }

    public void recognizeFish(Uri uri){
        mView.showProgress();
        File file = new File(uri.getEncodedPath());
        if (uri.toString().contains("content:/")) {
            file = new File(FileUtil.getInstance(mContext).getRealPathFromURI(mContext, uri));
        }

        // upload to backend first
        RequestBody fbody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image_file", file.getName(), fbody);

        mApiInterface.recognizeFish(filePart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Root>() {
            @Override
            public void onCompleted() {
                mView.dismissProgress();
                mView.showComplete();
            }


            @Override
            public void onError(Throwable e) {
                mView.dismissProgress();
                mView.onError(e.getMessage());

            }

            @Override
            public void onNext(Root status) {
                if(status.getStatus()){
                    mView.onContinueAction(status);
                }

            }
        });
    }

    public void createProduct(String title, Double price, String weight, String uri){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("title", title);
            jsonObject.put("current_price", price);
            jsonObject.put("seller_id", FishnetStorage.getInstance().getCurrentUserId());
            jsonObject.put("weight",weight);
            jsonObject.put("image_file", uri);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body =
                RequestBody.create(MediaType.parse("raw"), jsonObject.toString());

        mApiInterface.createProduct(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        mView.onError(e.getMessage());

                    }

                    @Override
                    public void onNext(Root root) {
                        mView.dismissProgress();

                        if (!root.getStatus())
                            mView.onError(root.getMessage());
                        else
                            mView.onNext(root.getMessage());


                    }
                });
    }

    public interface View {

        void onError(String error);

        void showComplete();

        void showProgress();

        void onNext(String message);

        void dismissProgress();

        void updateImageView(Uri uri);

        void onContinueAction(Root root);
    }
}
