package com.atos.fishnet.sell.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.utils.FileUtil;
import com.atos.fishnet.fishnet.utils.FocusView;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.camerakit.CameraKit;
import com.camerakit.CameraKitView;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.atos.fishnet.fishnet.utils.FocusView.INIT_TYPE_PARCELABLE;
import static com.atos.fishnet.fishnet.utils.FocusView.TYPE_ROUND;
import static com.atos.fishnet.fishnet.utils.FocusView.TYPE_SQUARE;

public class CameraKitActivity extends BaseActivity implements CameraKitView.ImageCallback  {

    public static final int CAMERAKIT_RESULT = 1220;

    @BindView(R.id.camera)
    CameraKitView cameraKitView;
    @BindView(R.id.btnDone)
    Button btnDone;

    @BindView(R.id.focusView)
    FocusView focusView;

    boolean cropIt = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_kit);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            isPermissionsToAccessStorageAreGranted(this);
        }

        int type = getIntent().getIntExtra(INIT_TYPE_PARCELABLE,TYPE_SQUARE);
        focusView.setFocusType(type);
        if(type==TYPE_SQUARE){
            cameraKitView.setFacing(CameraKit.FACING_BACK);
            cropIt = true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        cameraKitView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraKitView.onResume();
    }

    @Override
    protected void onPause() {
        cameraKitView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        cameraKitView.onStop();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        cameraKitView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnClick(R.id.btnDone)
    public void onCameraDoneClick(View view){
        cameraKitView.captureImage(this);
    }


    @Override
    public void onImage(CameraKitView cameraKitView, byte[] bytes) {
        new BitmapCompressor(bytes,cropIt).execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void isPermissionsToAccessStorageAreGranted(Activity activity) {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    class BitmapCompressor extends AsyncTask<Void,String,String> {

        byte[] initBytes;
        boolean mCrop;

        public BitmapCompressor(byte[] bytes,boolean cropIt){
            initBytes = bytes;
            mCrop = cropIt;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String result = "";

            Bitmap bitmap = BitmapFactory.decodeByteArray(initBytes, 0, initBytes.length);
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap,bitmap.getWidth()/2,bitmap.getHeight()/2,true);
            // crop it
            if(mCrop){
                if (scaled.getWidth() >= scaled.getHeight()){

                    scaled = Bitmap.createBitmap(
                            scaled,
                            scaled.getWidth()/2 - scaled.getHeight()/2,
                            0,
                            scaled.getHeight(),
                            scaled.getHeight()
                    );

                }else{

                    scaled = Bitmap.createBitmap(
                            scaled,
                            0,
                            scaled.getHeight()/2 - scaled.getWidth()/2,
                            scaled.getWidth(),
                            scaled.getWidth()
                    );
                }
            }

            try {
                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File directory = cw.getDir("fish", Context.MODE_PRIVATE);
                if (!directory.exists()) {
                    directory.mkdir();
                }
                File mypath = new File(directory, FileUtil.getUploadFileName());

                FileOutputStream out = new FileOutputStream(mypath);
                scaled.compress(Bitmap.CompressFormat.JPEG, 70, out);
                result = mypath.getAbsolutePath();
            }catch (Exception ex){
                ex.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(),"Something goes wrong",Toast.LENGTH_SHORT).show();
                return;
            }

            setResult(CAMERAKIT_RESULT,new Intent().setData(Uri.parse(result)));
            finish();

            super.onPostExecute(result);
        }
    }
}
