package com.atos.fishnet.sell.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageCaptureActivity extends BaseActivity {

    @BindView(R.id.img_capture)
    ImageView img_capture;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.fish_name)
    TextView fish_name;
    @BindView(R.id.red_container)
    LinearLayout red_container;
    @BindView(R.id.green_container)
    LinearLayout green_container;
    @BindView(R.id.orange_container)
    LinearLayout orange_container;
    @BindView(R.id.yellow_container)
    LinearLayout yellow_container;
    @BindView(R.id.img_back)
    ImageView img_back;


    protected Uri uri;
    Handler mHandler = new Handler(Looper.getMainLooper());

    protected static ImageCaptureActivity getInstance() {
        return new ImageCaptureActivity();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_capture);
        ButterKnife.bind(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Glide
                .with(this)
                .load(Uri.parse(getIntent().getExtras().getString("uri")))
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.bar_curve_white)
                .into(img_capture);

        if (getIntent().getExtras().getString("fish") != null)
            fish_name.setText(getIntent().getExtras().getString("fish"));

        if (getIntent().getExtras().getString("index").equalsIgnoreCase("Yellow")) {
            container.setBackgroundColor(getResources().getColor(R.color.yellow));
            red_container.setVisibility(View.GONE);
            green_container.setVisibility(View.GONE);
            orange_container.setVisibility(View.GONE);
            yellow_container.setVisibility(View.VISIBLE);
        } else if (getIntent().getExtras().getString("index").equalsIgnoreCase("Orange")) {
            container.setBackgroundColor(getResources().getColor(R.color.orange));
            red_container.setVisibility(View.GONE);
            green_container.setVisibility(View.GONE);
            orange_container.setVisibility(View.VISIBLE);
            yellow_container.setVisibility(View.GONE);
        } else if (getIntent().getExtras().getString("index").equalsIgnoreCase("Red")) {
            container.setBackgroundColor(getResources().getColor(R.color.red));
            red_container.setVisibility(View.VISIBLE);
            green_container.setVisibility(View.GONE);
            orange_container.setVisibility(View.GONE);
            yellow_container.setVisibility(View.GONE);
        } else if (getIntent().getExtras().getString("index").equalsIgnoreCase("Green")) {
            container.setBackgroundColor(getResources().getColor(R.color.green));
            red_container.setVisibility(View.GONE);
            green_container.setVisibility(View.VISIBLE);
            orange_container.setVisibility(View.GONE);
            yellow_container.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.btn_proceed_green,R.id.btn_proceed_yellow})
    public void onProceed(){
        Intent intent = new Intent(ImageCaptureActivity.this,AddProductActivity.class);
        intent.putExtra("uri",getIntent().getExtras().getString("uri"));
        intent.putExtra("name",getIntent().getExtras().getString("fish"));
        startActivity(intent);
    }
}
