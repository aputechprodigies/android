package com.atos.fishnet.onboarding.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.airbnb.lottie.LottieAnimationView;
import com.atos.fishnet.R;

public class SliderAdapter extends PagerAdapter {

    Context context;
    Activity activity;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    public Integer[] slideImages = {
            R.drawable.ic_undraw_ai,
           R.drawable.ic_undraw_chatbot,
            R.drawable.ic_undraw_wallet_onboard
    };

    public String[] slideHeadings = {
            "Cooperative Artificial Intelligence",
            "ChatBot AI",
            "E-Wallet Payment"
    };

    public String[] slideDescriptions = {
            "Seamlessly upload a picture of your fish and let our technology help you identify and analyse it.",
            "Got any questions ? Question our FishBot!. Clear all doubts!",
            "Make deposits and payments to and from your FishPay E-Wallet account easily with our trustworthy secured technology."
    };


    @Override
    public int getCount() {
        return slideHeadings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_slide, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.iv_image_icon);
        TextView slideHeading = (TextView) view.findViewById(R.id.tv_heading);
        TextView slideDescription = (TextView) view.findViewById(R.id.tv_description);

        slideImageView.setImageResource(slideImages[position]);
        slideHeading.setText(slideHeadings[position]);
        slideDescription.setText(slideDescriptions[position]);

        container.addView(view);

        return view;

    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);  //todo: RelativeLayout??
    }
}
