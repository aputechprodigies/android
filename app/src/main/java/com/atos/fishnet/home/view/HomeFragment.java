package com.atos.fishnet.home.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieDrawable;
import com.atos.fishnet.R;
import com.atos.fishnet.SwipeRefresh.PullToRefreshLayout;
import com.atos.fishnet.SwipeRefresh.PullToRefreshListener;
import com.atos.fishnet.chatbot.view.ChatSupportActivity;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.eWallet.view.InputAmountActivity;
import com.atos.fishnet.fishnet.view.MainFragment;
import com.atos.fishnet.home.model.WeatherDetails;
import com.atos.fishnet.home.presenter.HomeContract;
import com.atos.fishnet.home.presenter.HomePresenterImpl;
import com.atos.fishnet.knowledgebase.view.KnowledgeBaseActivity;
import com.atos.fishnet.purchase.view.PurchaseProductActivity;
import com.atos.fishnet.sell.view.SellProductActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends MainFragment implements HomeContract.View {

    @BindView(R.id.iv_image_icon)
    LottieAnimationView iv_image_icon;
    @BindView(R.id.purchase_container)
    LinearLayout purchase_container;
    @BindView(R.id.sell_container)
    LinearLayout sell_container;
    @BindView(R.id.knowledge_base_container)
    LinearLayout knowledge_base_container;
    @BindView(R.id.top_up_container)
    LinearLayout top_up_container;
    @BindView(R.id.chat_support_container)
    LinearLayout chat_support_container;
    @BindView(R.id.txt_balance)
    TextView txt_balance;
    @BindView(R.id.tv_location)
    TextView txt_location;
    @BindView(R.id.tv_weather)
    TextView txt_weather;
    @BindView(R.id.pull_refresh)
    PullToRefreshLayout pull_refresh;

    private Unbinder mUnBinder;
    int PERMISSION_ID = 44;
    FusedLocationProviderClient mFusedLocationClient;
    HomePresenterImpl homePresenter;


    public static HomeFragment getInstance(){
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        homePresenter = new HomePresenterImpl(this);
        purchase_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeFragment.this.getActivity(), PurchaseProductActivity.class));
            }
        });

        sell_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeFragment.this.getActivity(), SellProductActivity.class));

            }
        });

        knowledge_base_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeFragment.this.getActivity(), KnowledgeBaseActivity.class));

            }
        });

        top_up_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeFragment.this.getActivity(), InputAmountActivity.class));

            }
        });

        chat_support_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeFragment.this.getActivity(), ChatSupportActivity.class));
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        pull_refresh.setPullToRefreshListener(new PullToRefreshListener() {
            @Override
            public void onStartRefresh(@Nullable View view) {
                homePresenter.onGetBalance();
            }
        });

        getLastLocation();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        homePresenter.onGetBalance();
    }

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
              getLastLocation();
            else
                Toast.makeText(getContext(),"Does not be able to retrieve the current location",Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null)
                                    requestNewLocationData();
                                else
                                    homePresenter.onFetchWeatherDetails(Double.toString(location.getLatitude()),Double.toString(location.getLongitude()));
                            }
                        }
                );
            } else {
                Toast.makeText(getContext(), "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else
            requestPermissions();
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(6000);
        mLocationRequest.setFastestInterval(4000);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
        }
    };


    @Override
    public void onNextWeather(String message, WeatherDetails weatherDetails) {

        txt_weather.setText(weatherDetails.getWeather());
        String location = weatherDetails.getLocation()+", "+weatherDetails.getCountry()+ " ";
        txt_location.setText(location);

        iv_image_icon.setAnimation("4800-weather-partly-cloudy.json");
        iv_image_icon.loop(true);
        iv_image_icon.playAnimation();
        switch (weatherDetails.getWeather())
        {
            case "Rain":
            case "Drizzle":
                initLottieWeather("4801-weather-partly-shower.json");
                break;
            case "Thunderstorm":
                initLottieWeather("4805-weather-thunder.json");
                break;
            case "Snow":
                initLottieWeather("4793-weather-snow.json");
                break;
            case "Clear":
                initLottieWeather("4804-weather-sunny.json");
                break;
            case "Clouds":
                initLottieWeather("4800-weather-partly-cloudy.json");
                    break;
            default:
                initLottieWeather("4795-weather-mist.json");
                break;
        }
    }

    private void initLottieWeather(String fileName){
        iv_image_icon.setAnimation(fileName);
        iv_image_icon.loop(true);
        iv_image_icon.playAnimation();
    }

    @Override
    public void showWeatherError(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT);
    }

    @Override
    public void onFetchBalance(String balance) {
        pull_refresh.refreshDone();
        FishnetStorage.getInstance().setWalletBalance(balance);
        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatCurrency = new DecimalFormatSymbols();

        formatCurrency.setCurrencySymbol("RM ");
        formatCurrency.setMonetaryDecimalSeparator('.');
        formatCurrency.setGroupingSeparator(',');

        df.setDecimalFormatSymbols(formatCurrency);
        txt_balance.setText(df.format(Double.valueOf(FishnetStorage.getInstance().getWalletBalance())));
    }
}
