package com.atos.fishnet.home.model;

import com.google.gson.annotations.SerializedName;

public class WeatherDetails {
    @SerializedName("weather")
    private String weather;
    @SerializedName("description")
    private String description;
    @SerializedName("country")
    private String country;
    @SerializedName("location")
    private String location;

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
