package com.atos.fishnet.home.presenter;

import android.util.Log;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.home.view.HomeFragment;
import com.atos.fishnet.model.Root;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomePresenterImpl {
    private HomeFragment homeFragment;
    private APIInterface mAPIInterface;


    public HomePresenterImpl(HomeFragment view) {
        this.homeFragment = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public void onFetchWeatherDetails(String latitude, String longitude) {
        JSONObject object = new JSONObject();
        try {
            object.put("lat", latitude);
            object.put("long", longitude);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.getWeather(FishnetStorage.getInstance().getAuthToken(), body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {

                    }
                    @Override
                    public void onError(Throwable e) {
                        homeFragment.showWeatherError(e.toString());
                    }
                    @Override
                    public void onNext(Root root) {
                        homeFragment.onNextWeather(root.getMessage(),root.getWeatherDetails());
                    }
                });
    }

    public void onGetBalance(){
        JSONObject object = new JSONObject();
        try {
            object.put("id", FishnetStorage.getInstance().getCurrentUserId());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.getBalance(FishnetStorage.getInstance().getAuthToken(),body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {

                    }
                    @Override
                    public void onError(Throwable e) {
                        homeFragment.showWeatherError(e.toString());
                    }
                    @Override
                    public void onNext(Root root) {
                        homeFragment.onFetchBalance(root.getBalance());
                    }
                });
    }

}
