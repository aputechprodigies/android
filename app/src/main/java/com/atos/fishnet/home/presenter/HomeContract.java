package com.atos.fishnet.home.presenter;

import com.atos.fishnet.home.model.WeatherDetails;

public interface HomeContract {

    interface View {
        void onNextWeather(String message, WeatherDetails weatherDetails);
        void showWeatherError(String msg);
        void onFetchBalance(String balance);
    }
}
