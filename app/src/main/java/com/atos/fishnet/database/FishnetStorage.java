package com.atos.fishnet.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class FishnetStorage {
    private static final FishnetStorage sInstance = new FishnetStorage();
    private static final String PREF_LOGGED_IN = "logged_in";
    private static final String PREF_SKIPPED_REGISTRATION = "skip_registration";
    private static final String PREF_FIRST_TIME = "first_time";
    private static final String PREF_AUTH_TOKEN = "auth_token";
    private static final String PREF_USER_ID = "user_id";
    private static final String PREF_USER_NAME = "user_name";
    private static final String PREF_USER_WALLET_BALANCE = "balance";
    private static final String PREF_CREDIT_CARD_ADDED="credit_card";

    private SharedPreferences mSharedPreferences;

    public static synchronized FishnetStorage getInstance() {
        return FishnetStorage.sInstance;
    }

    public FishnetStorage() {
        super();
    }

    public void init(final Context context) {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setLoggedIn(final boolean flag) {
        setBoolean(PREF_LOGGED_IN, flag);
    }

    public boolean getIsLoggedIn() {
        return getBoolean(PREF_LOGGED_IN);
    }

    public void setSkipRegistration(final boolean flag) {
        setBoolean(PREF_SKIPPED_REGISTRATION, flag);
    }

    public boolean getIsCreditCardAdded() {
        return getBoolean(PREF_CREDIT_CARD_ADDED);
    }

    public void setCreditCardAdded(final boolean flag) {
        setBoolean(PREF_CREDIT_CARD_ADDED, flag);
    }

    public boolean getIsSkipRegistration() {
        return getBoolean(PREF_SKIPPED_REGISTRATION);
    }

    public void setFirstTimeUser(final Boolean flag) {
        setBoolean(PREF_FIRST_TIME, flag);
    }

    public Boolean isFirstTimeUser() {
        return getBoolean(PREF_FIRST_TIME);
    }

    public void setAuthToken(final String token) {
        setString(PREF_AUTH_TOKEN, token);
    }

    public String getAuthToken() {
        return getString(PREF_AUTH_TOKEN);
    }

    public void setCurrentUserId(final String userId) {
        setString(PREF_USER_ID, userId);
    }

    public String getCurrentUserId() {
        return getString(PREF_USER_ID);
    }

    public void setCurrentUserName(final String userName) {
        setString(PREF_USER_NAME, userName);
    }

    public String getCurrentUserName() {
        return getString(PREF_USER_NAME);
    }

    public void setWalletBalance(final String walletBalance) {
        setString(PREF_USER_WALLET_BALANCE, walletBalance);
    }

    public String getWalletBalance() {
        return getString(PREF_USER_WALLET_BALANCE);
    }


    /********************************************
     * *************** CONVENIENCE **************
     * ******************************************
     */

    private void setBoolean(final String key, final boolean value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putBoolean(key, value).apply();
        }
    }

    private boolean getBoolean(final String key) {
        return mSharedPreferences != null && mSharedPreferences.getBoolean(key, false);
    }

    private void setInt(final String key, final int value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putInt(key, value).apply();
        }
    }

    private int getInt(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getInt(key, 0);
        } else {
            return 0;
        }
    }

    private void setLong(final String key, final long value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putLong(key, value).apply();
        }
    }

    private long getLong(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getLong(key, 0);
        } else {
            return 0;
        }
    }

    private void setString(final String key, final String value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putString(key, value).apply();
        }
    }

    private String getString(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getString(key, null);
        } else {
            return null;
        }
    }

    private void setFloat(final String key, final float value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putFloat(key, value).apply();
        }
    }

    private float getFloat(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getFloat(key, 0);
        } else {
            return 0;
        }
    }
}
