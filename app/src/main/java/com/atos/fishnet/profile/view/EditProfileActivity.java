package com.atos.fishnet.profile.view;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.txt_first_name)
    TextView txt_first_name;
    @BindView(R.id.txt_last_name)
    TextView txt_last_name;
    @BindView(R.id.txt_date_of_birth)
    TextView txt_date_of_birth;
    @BindView(R.id.txt_address_line_1)
    TextView txt_address_line_1;
    @BindView(R.id.txt_address_line_2)
    TextView txt_address_line_2;
    @BindView(R.id.txt_address_locality)
    TextView txt_address_locality;
    @BindView(R.id.txt_address_postal)
    TextView txt_address_postal;
    @BindView(R.id.txt_address_country)
    TextView txt_address_country;
    @BindView(R.id.txt_phone_number)
    TextView txt_phone_number;
    @BindView(R.id.txt_about_you)
    TextView txt_about_you;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
    }
}
