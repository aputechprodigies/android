package com.atos.fishnet.profile.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.fishnet.view.MainFragment;
import com.atos.fishnet.login.view.LoginActivity;
import com.atos.fishnet.purchase.view.ItemsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProfileFragment extends MainFragment {

    private Unbinder mUnBinder;

    @BindView(R.id.txt_profile)
    TextView txt_profile;
    @BindView(R.id.edit_profile_container)
    LinearLayout edit_profile_container;
    @BindView(R.id.logout_container)
    LinearLayout logout_container;

    public static ProfileFragment getInstance(){
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        txt_profile.setText(FishnetStorage.getInstance().getCurrentUserName());

        edit_profile_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileFragment.this.getContext(),EditProfileActivity.class));
            }
        });

        logout_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // clear shared preferences for auth
                FishnetStorage.getInstance().setAuthToken(null);
                FishnetStorage.getInstance().setSkipRegistration(false);
                FishnetStorage.getInstance().setCurrentUserId(null);
                FishnetStorage.getInstance().setCurrentUserName(null);
                FishnetStorage.getInstance().setWalletBalance(null);
                FishnetStorage.getInstance().setCreditCardAdded(false);
                FishnetStorage.getInstance().setLoggedIn(false);
                FishnetStorage.getInstance().setFirstTimeUser(false);

                if (ProfileFragment.this.getActivity() != null) {
                    ProfileFragment.this.getActivity().finish();
                    startActivity(new Intent(ProfileFragment.this.getActivity(), LoginActivity.class));
                }
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }
}
