package com.atos.fishnet.SwipeRefresh;

import android.view.View;

import androidx.annotation.Nullable;

public interface PullToRefreshListener {
    void onStartRefresh(@Nullable View view);
}
