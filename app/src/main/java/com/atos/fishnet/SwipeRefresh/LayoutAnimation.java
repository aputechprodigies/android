package com.atos.fishnet.SwipeRefresh;

import android.view.View;
import android.view.animation.Animation;

import java.lang.ref.WeakReference;

public class LayoutAnimation extends Animation {
    private static final int DEFAULT_REFRESH_LAYOUT_ANIMATION_DURATION = 300;

    WeakReference<View> targetViewWeakRef;
    WeakReference<ChildViewTopMarginCalculator> childViewTopMarginCalculatorWeakRef;
    int refreshLayoutHeight;

    LayoutAnimation(ChildViewTopMarginCalculator childViewTopMarginCalculator, View targetView,
                    int refreshLayoutHeight) {
        childViewTopMarginCalculatorWeakRef = new WeakReference<>(childViewTopMarginCalculator);
        targetViewWeakRef = new WeakReference<>(targetView);
        this.refreshLayoutHeight = refreshLayoutHeight;
        setDuration(DEFAULT_REFRESH_LAYOUT_ANIMATION_DURATION);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}