package com.atos.fishnet.SwipeRefresh;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.pnikosis.materialishprogress.ProgressWheel;

public class DefaultRefreshIcon implements RefreshIcon {
    private ProgressWheel refreshIcon;

    public DefaultRefreshIcon(@NonNull Context context, int refreshIconColor, int refreshIconSize,
                              int refreshIconSpinDuration) {
        refreshIcon = new ProgressWheel(context);
        RelativeLayout.LayoutParams refreshIconLayoutParams = new RelativeLayout.LayoutParams(refreshIconSize,
                refreshIconSize);
        refreshIconLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        setSpinSpeed(refreshIconSpinDuration);
        refreshIcon.setLayoutParams(refreshIconLayoutParams);
        refreshIcon.setBarColor(refreshIconColor);
        refreshIcon.setCircleRadius(refreshIconSize);
        refreshIcon.setProgress(0);
    }

    public void setRefreshIconColor(int refreshIconColor) {
        refreshIcon.setBarColor(refreshIconColor);
    }

    public void setRefreshIconSize(int refreshIconSize) {
        refreshIcon.setCircleRadius(refreshIconSize);
    }

    @NonNull
    @Override
    public View getIconView() {
        return refreshIcon;
    }

    @Override
    public void setProgress(float progress) {
        if (refreshIcon.isSpinning()) {
            refreshIcon.setInstantProgress(progress);
        } else {
            refreshIcon.setProgress(progress);
        }
    }

    @Override
    public void spin() {
        refreshIcon.spin();
    }

    @Override
    public void setSpinSpeed(int spinSpeed) {
        refreshIcon.setSpinSpeed(1 / (spinSpeed / 1000f));
    }

    @Override
    public boolean isSpinning() {
        return refreshIcon.isSpinning();
    }
}