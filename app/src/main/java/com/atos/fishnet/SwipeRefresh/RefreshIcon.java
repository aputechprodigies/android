package com.atos.fishnet.SwipeRefresh;

import android.view.View;

import androidx.annotation.NonNull;

public interface RefreshIcon {
    @NonNull
    View getIconView();
    void setProgress(float progress);
    void spin();
    void setSpinSpeed(int spinSpeed);
    boolean isSpinning();
}
