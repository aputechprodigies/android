package com.atos.fishnet.SwipeRefresh;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class RefreshIconFactory {
    public static RefreshIcon createRefreshIcon(@NonNull Context context, int refreshIconColor, int refreshIconSize,
                                                int refreshIconSpinDuration, @Nullable Drawable refreshIconDrawable) {
        if (refreshIconDrawable == null)
            return new DefaultRefreshIcon(context, refreshIconColor, refreshIconSize, refreshIconSpinDuration);
        else
            return new DrawableRefreshIcon(context, refreshIconDrawable, refreshIconSize, refreshIconSpinDuration);
    }
}
