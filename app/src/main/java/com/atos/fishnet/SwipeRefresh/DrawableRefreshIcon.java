package com.atos.fishnet.SwipeRefresh;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

public class DrawableRefreshIcon implements RefreshIcon {
    private ImageView imageView;
    private boolean isSpinning = false;
    private int refreshIconSpinDuration;

    public DrawableRefreshIcon(@NonNull Context context, @NonNull Drawable refreshIconDrawable, int refreshIconSize,
                               int refreshIconSpinDuration) {
        imageView = new ImageView(context);
        RelativeLayout.LayoutParams progressBarLayoutParams = new RelativeLayout.LayoutParams(refreshIconSize,
                refreshIconSize);
        progressBarLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        imageView.setLayoutParams(progressBarLayoutParams);
        imageView.setImageDrawable(refreshIconDrawable);
        this.refreshIconSpinDuration = refreshIconSpinDuration;
    }

    @NonNull
    @Override
    public View getIconView() {
        return imageView;
    }

    @Override
    public void setProgress(float progress) {
        isSpinning = false;
        imageView.clearAnimation();
        imageView.setRotation(progress * 360);
    }

    @Override
    public void spin() {
        isSpinning = true;
        imageView.clearAnimation();
        imageView.startAnimation(createSpinningAnimation());
    }

    @Override
    public void setSpinSpeed(int spinSpeed) {
        refreshIconSpinDuration = spinSpeed;
    }

    private Animation createSpinningAnimation() {
        RotateAnimation rotateAnimation = new RotateAnimation(imageView.getRotation(), imageView.getRotation() + 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setRepeatMode(Animation.RESTART);
        rotateAnimation.setDuration(refreshIconSpinDuration);
        rotateAnimation.setInterpolator(new LinearInterpolator());

        return rotateAnimation;
    }

    @Override
    public boolean isSpinning() {
        return isSpinning;
    }
}