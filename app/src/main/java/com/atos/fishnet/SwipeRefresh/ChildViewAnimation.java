package com.atos.fishnet.SwipeRefresh;

import android.service.autofill.Transformation;
import android.view.View;
import android.view.ViewGroup;

public class ChildViewAnimation extends LayoutAnimation {

    public ChildViewAnimation(ChildViewTopMarginCalculator childViewTopMarginCalculator, View targetView,
                              int refreshLayoutHeight) {
        super(childViewTopMarginCalculator, targetView, refreshLayoutHeight);
    }

    protected void applyTransformation(float interpolatedTime, Transformation t) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) targetViewWeakRef.get().getLayoutParams();
        params.topMargin = childViewTopMarginCalculatorWeakRef.get()
                .calculateNewTopMarginAtInterpolatedTime(refreshLayoutHeight, interpolatedTime);
        targetViewWeakRef.get().setLayoutParams(params);
    }
}