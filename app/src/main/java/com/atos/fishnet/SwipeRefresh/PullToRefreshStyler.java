package com.atos.fishnet.SwipeRefresh;

import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface PullToRefreshStyler {

    void setPullToRefreshListener(@NonNull PullToRefreshListener pullToRefreshListener);

    /**
     * Sets padding for the refresh layout.
     * Default value is 4dp.
     *
     * @param refreshLayoutPadding padding for refresh layout
     */
    void setRefreshLayoutPadding(int refreshLayoutPadding);

    /**
     * Sets the background color for the refresh layout.
     * Default value is Color.LTGRAY.
     *
     * @param refreshLayoutBackgroundColor background color for the refresh layout
     */
    void setRefreshLayoutBackgroundColor(int refreshLayoutBackgroundColor);

    /**
     * Sets the color for the refresh icon.
     * This value is ignored if refreshIconDrawable is set.
     * Default value is Color.DKGRAY.
     *
     * @param refreshIconColor color of the {@link DefaultRefreshIcon}
     */
    void setRefreshIconColor(int refreshIconColor);

    /**
     * Sets the size of the refresh icon.
     * Default value is 24dp.
     *
     * @param refreshIconSizeInPx size of the refresh icon in px
     */
    void setRefreshIconSize(int refreshIconSizeInPx);

    /**
     * Sets the duration of one full spin of the refresh icon.
     * Default value is 800ms.
     *
     * @param refreshIconSpinDuration duration of one full spin of the refresh icon in ms
     */
    void setRefreshIconSpinDuration(int refreshIconSpinDuration);

    /**
     * Sets the drawable for the refresh icon.
     *
     * @param refreshIconDrawable drawable to be used for the refresh icon
     */
    void setRefreshIconDrawable(@Nullable Drawable refreshIconDrawable);

    /**
     * Sets custom view for the refresh icon.
     *
     * @param refreshIcon object for custom refresh icon which implements {@link RefreshIcon}
     */
    void setRefreshIcon(@NonNull RefreshIcon refreshIcon);

    /**
     * Sets max height of the refresh layout.
     * Default value is 500dp.
     *
     * @param refreshLayoutMaxHeightInPx max height of the refresh layout in px.
     */
    void setRefreshLayoutMaxHeight(int refreshLayoutMaxHeightInPx);

    /**
     * Sets threshold height of the refresh layout to start refreshing.
     * Default value is 300dp.
     *
     * @param refreshLayoutThresholdHeightInPx threshold height of the refresh layout in px.
     */
    void setRefreshLayoutThresholdHeight(int refreshLayoutThresholdHeightInPx);

    /**
     * Sets whether to block user-driven scroll event while refreshing.
     * Default value is true
     *
     * @param blockScrollWhileRefreshing true to block scroll
     */
    void setBlockScrollWhileRefreshing(boolean blockScrollWhileRefreshing);
}