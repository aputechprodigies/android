package com.atos.fishnet.SwipeRefresh;

public interface ChildViewTopMarginCalculator {
    int calculateNewTopMarginAtInterpolatedTime(int refreshLayoutHeight, float interpolatedTime);
    int getInitialTopMargin();
}
