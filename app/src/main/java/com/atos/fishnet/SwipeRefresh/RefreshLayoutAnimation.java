package com.atos.fishnet.SwipeRefresh;

import android.view.View;
import android.view.animation.Transformation;

public class RefreshLayoutAnimation extends LayoutAnimation {
    public RefreshLayoutAnimation(ChildViewTopMarginCalculator childViewTopMarginCalculator, View targetView,
                                  int refreshLayoutHeight) {
        super(childViewTopMarginCalculator, targetView, refreshLayoutHeight);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        targetViewWeakRef.get().getLayoutParams().height = childViewTopMarginCalculatorWeakRef.get()
                .calculateNewTopMarginAtInterpolatedTime(refreshLayoutHeight,
                        interpolatedTime) - childViewTopMarginCalculatorWeakRef.get().getInitialTopMargin();
    }
}