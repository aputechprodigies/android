package com.atos.fishnet.chatbot.presenter;

import android.net.Uri;

import com.atos.fishnet.chatbot.view.ChatSupportActivity;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.fishnet.FishnetApplication;
import com.atos.fishnet.fishnet.utils.FileUtil;
import com.atos.fishnet.model.Root;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChatSupportPresenterImpl {

    private ChatSupportActivity chatSupportActivity;
    private APIInterface mAPIInterface;

    public ChatSupportPresenterImpl(ChatSupportActivity view) {
        this.chatSupportActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public void sendMessage(String data, File file){

        MultipartBody.Part filePart = null;
        if(file!=null) {
            // upload to backend first
            RequestBody fbody = RequestBody.create(MediaType.parse("image/jpeg"), file);
            filePart = MultipartBody.Part.createFormData("image_file", file.getName(), fbody);
        }

        mAPIInterface.sendMessage(RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(data)),filePart).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Root root) {
                        chatSupportActivity.receiveMessage(root.getData(), root.getUrl());
                    }

                });
    }
}
