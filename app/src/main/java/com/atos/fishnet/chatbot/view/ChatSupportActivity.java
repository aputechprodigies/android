package com.atos.fishnet.chatbot.view;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.atos.fishnet.R;
import com.atos.fishnet.chatbot.data.ChatView;
import com.atos.fishnet.chatbot.data.Message;
import com.atos.fishnet.chatbot.presenter.ChatSupportPresenterImpl;
import com.atos.fishnet.fishnet.utils.ChoosePhoto;
import com.atos.fishnet.fishnet.utils.FileUtil;
import com.atos.fishnet.fishnet.utils.PermissionUtil;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.github.zagum.expandicon.ExpandIconView;
import com.zhihu.matisse.Matisse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatSupportActivity extends BaseActivity {

    @BindView(R.id.chatView)
    ChatView chatView;

    public static int imagePickerRequestCode = 10;
    public static int SELECT_VIDEO = 11;
    public static int CAMERA_REQUEST = 12;
    private ChatSupportPresenterImpl mPresenter;

    boolean switchbool = true;
    boolean more = false;
    List<Uri> mSelected;

    EditText messageET;
    TextToSpeech t1;

    @BindView(R.id.img_back)
    ImageView imgBack;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_support);
        ButterKnife.bind(this);
        mPresenter = new ChatSupportPresenterImpl(this);
        mSelected = new ArrayList<>();
        PermissionUtil permissionUtil = new PermissionUtil();

        if (permissionUtil.checkMarshMellowPermission()) {
            if (permissionUtil.verifyPermissions(this, permissionUtil.getCameraPermissions())){

            }
            else {
                ActivityCompat.requestPermissions((Activity) this, permissionUtil.getCameraPermissions(), 103);
            }
        } else {
            // showAlertDialog();
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // this code will be executed after 2 seconds
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initMessage();
                    }
                });
            }
        }, 1000);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ENGLISH);
                }
            }
        });

        chatView.setOnClickSpeechButtonListener(new ChatView.OnClickSpeechButtonListener() {
            @Override
            public void onSpeechButtonClick() {
                getSpeechInput();
            }
        });
        //Send button click listener
        chatView.setOnClickSendButtonListener(new ChatView.OnClickSendButtonListener() {
            @Override
            public void onSendButtonClick(String body) {
                if(!body.equals("")) {
                    mPresenter.sendMessage(body, null);
                    Message message = new Message();
                    message.setBody(body);
                    message.setMessageType(Message.MessageType.RightSimpleImage);
                    message.setTime(getTime());
                    message.setUserName("Vishanth");
                    message.setUserIcon(Uri.parse(""));
                    chatView.addMessage(message);
                }
                switchbool = false;
            }
        });

        //Camera button click listener
        chatView.setOnClickCameraButtonListener(new ChatView.OnClickCameraButtonListener() {
            @Override
            public void onCameraButtonClicked() {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
                file.delete();
                File file1 = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");

                Uri uri = FileProvider.getUriForFile(ChatSupportActivity.this, getApplicationContext().getPackageName() + ".provider", file1);
                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        t1.stop();
    }

    public void getSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Input", Toast.LENGTH_SHORT).show();
        }
    }

    private void initMessage() {
        Message message1 = new Message();
        message1.setBody("Hi, I am FishBOT. Your assistant in FishNET. How may I assist you today ?");
        message1.setMessageType(Message.MessageType.LeftSimpleMessage);
        message1.setTime(getTime());
        message1.setUserName("FishBOT");
        message1.setUserIcon(Uri.parse(""));
        chatView.addMessage(message1);
        t1.speak("Hi, I am FishBOT. Your assistant in FishNET. How may I assist you today ?", TextToSpeech.QUEUE_FLUSH, null);
    }

    public void receiveMessage(String body, String url) {
        Message message1 = new Message();
        message1.setBody(body);
        message1.setMessageType(Message.MessageType.LeftSimpleMessage);
        message1.setTime(getTime());
        message1.setUserName("FishBOT");
        message1.setUserIcon(Uri.parse(""));
        chatView.addMessage(message1);
        t1.speak(body, TextToSpeech.QUEUE_FLUSH, null);

        if (url != null) {
            Message message = new Message();
            message.setMessageType(Message.MessageType.LeftSingleImage);
            message.setTime(getTime());
            message.setUserName("FishBOT");
            mSelected.clear();
            mSelected.add(Uri.parse(url));
            message.setImageList(mSelected);
            message.setUserIcon(Uri.parse(""));
            chatView.addMessage(message);
            switchbool = false;
        }
    }

    public String getTime() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd MMM yyyy HH:mm");
        String time = mdformat.format(calendar.getTime());
        return time;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

           //speech Message Result
                    if (requestCode==10 && resultCode == RESULT_OK && data != null) {
                        ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        mPresenter.sendMessage(result.get(0),null);
                        Message message = new Message();
                        message.setBody(result.get(0));
                        message.setMessageType(Message.MessageType.RightSimpleImage);
                        message.setTime(getTime());
                        message.setUserName("Vishanth");
                        message.setUserIcon(Uri.parse(""));
                        chatView.addMessage(message);
                        switchbool=false;
                    }

                    //Image Capture result
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

                Message message = new Message();
                message.setMessageType(Message.MessageType.RightSingleImage);
                message.setTime(getTime());
                message.setUserName("Vishanth");
                mSelected.clear();
                File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
                //Uri of camera image
                Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);

                ContentResolver cr = getContentResolver();
                getContentResolver().notifyChange(uri, null);
                Bitmap bitmap = null;
                try {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2, true);
                    if (scaled.getWidth() >= scaled.getHeight()) {

                        scaled = Bitmap.createBitmap(
                                scaled,
                                scaled.getWidth() / 2 - scaled.getHeight() / 2,
                                0,
                                scaled.getHeight(),
                                scaled.getHeight()
                        );

                    } else {

                        scaled = Bitmap.createBitmap(
                                scaled,
                                0,
                                scaled.getHeight() / 2 - scaled.getWidth() / 2,
                                scaled.getWidth(),
                                scaled.getWidth()
                        );
                    }

                    ContextWrapper cw = new ContextWrapper(getApplicationContext());
                    File directory = cw.getDir("fish", Context.MODE_PRIVATE);
                    if (!directory.exists()) {
                        directory.mkdir();
                    }
                    File mypath = new File(directory, FileUtil.getUploadFileName());

                    FileOutputStream out = new FileOutputStream(mypath);
                    scaled.compress(Bitmap.CompressFormat.JPEG, 70, out);
                    mPresenter.sendMessage(null, mypath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mSelected.add(uri);
                message.setImageList(mSelected);
                message.setUserIcon(Uri.parse(""));
                chatView.addMessage(message);
                switchbool = false;
            }
        }

}
