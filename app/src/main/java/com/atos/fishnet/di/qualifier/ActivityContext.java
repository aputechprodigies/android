package com.atos.fishnet.di.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ActivityContext {
}
