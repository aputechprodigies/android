package com.atos.fishnet.di.component;

import android.content.Context;


import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.ContextModule;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.di.qualifier.ApplicationContext;
import com.atos.fishnet.di.scope.ApplicationScope;
import com.atos.fishnet.fishnet.FishnetApplication;

import dagger.Component;


@ApplicationScope
    @Component(modules = {ContextModule.class, RetrofitModule.class})
    public interface ApplicationComponent {
        APIInterface getApiInterface();

        @ApplicationContext
        Context getContext();

        void injectApplication(FishnetApplication myApplication);
    }


