package com.atos.fishnet.di.module;

import android.content.Context;


import com.atos.fishnet.di.qualifier.ApplicationContext;
import com.atos.fishnet.di.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context provideContext() {
        return context;
    }
}
