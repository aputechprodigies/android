package com.atos.fishnet.di.module;

import com.atos.fishnet.eWallet.model.EwalletModel;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.notification.model.Notification;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface APIInterface {

    /**
     * Users Endpoints
     **/
    @POST("users/register")
    Observable<Root> signUpByEmail(@Body RequestBody requestBody);

    @POST("users/authenticate")
    Observable<Root> signInByEmail(@Body RequestBody requestBody);


    /**
     * E-Wallet Endpoints
     **/
    @GET("wallet/qr")
    Observable<EwalletModel> get_qr(@Header("x-access-token") String token, @Query("id") String id);

    @POST("wallet/scan")
    Observable<EwalletModel> scan_qr(@Header("x-access-token") String token, @Body RequestBody requestBody);

    @POST("wallet/topup")
    Observable<EwalletModel> topup(@Header("x-access-token") String token, @Body RequestBody requestBody);

    @POST("wallet/balance")
    Observable<Root> getBalance(@Header("x-access-token") String token,@Body RequestBody requestBody);

    @POST("wallet/purchase")
    Observable<Root> purchaseProduct(@Header("x-access-token") String token,@Body RequestBody requestBody);


    /**
     * Fishnet Machine Learning Endpoints
     **/
    @GET("/fish/all/{spectrum}")
    Observable<Root> getFishInfo(@Header("x-access-token") String authToken,@Path("spectrum") String spectrum);

    @POST("fish/weather")
    Observable<Root>getWeather(@Header("x-access-token") String authToken, @Body RequestBody requestBody);

    @POST("recognition/")
    @Multipart
    Observable<Root> recognizeFish(@Part MultipartBody.Part file);


    /**
     * Product Endpoints
     **/
    @POST("/product/fish")
    Observable<Root> createProduct(@Body RequestBody object);

    @GET("/product/fish")
    Observable<Root> getProductList(@Query("id") String id);

    @GET("product/fish/{id}")
    Observable<Root> getProductDetails(@Path("id") String id);

    @PUT("product/fish/{fishID}")
    @Multipart
    Observable<Root> uploadImage( @Path("fishID") String id, @Part MultipartBody.Part file);


    /**
     * ChatBot Endpoint
     **/
    @POST("/chatbot/")
    @Multipart
    Observable<Root> sendMessage( @Part("input_text") RequestBody input_text,@Part MultipartBody.Part image_file);


    /**
     * Payment Endpoints
     **/
    @POST("payment/creditcard")
    Observable<Root> createCreditCard( @Body RequestBody object);

    @GET("payment/creditcard/{id}")
    Observable<Root>getCreditCard( @Path("id") String id);

    @DELETE("payment/creditcard/{id}")
    Observable<Root>deleteCreditCard(@Path("id") String id);


    /**
     * Notification Endpoints
     **/
    @POST("notification/send")
    Observable<Root>sendNotification();

    @POST("notification/")
    Observable<Notification>getNotification(@Body RequestBody body);

    @GET("notification/all")
    Observable<Root>getAllNotification();
}
