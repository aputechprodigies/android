package com.atos.fishnet.purchase.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.atos.fishnet.purchase.model.SellerInfo;
import com.atos.fishnet.purchase.presenter.PurchaseProductPresenterImpl;
import com.atos.fishnet.sell.model.Product;
import com.github.islamkhsh.CardSliderViewPager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PurchaseProductActivity extends BaseActivity implements PurchaseProductPresenterImpl.View, ItemsAdapter.ItemClickListener{

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    private ItemsAdapter itemsAdapter;

    PurchaseProductPresenterImpl mPresenter;

    @BindView(R.id.img_back)
    ImageView img_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_product);
        ButterKnife.bind(this);
        mPresenter = new PurchaseProductPresenterImpl(this,this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        CardSliderViewPager cardSliderViewPager = (CardSliderViewPager) findViewById(R.id.viewPager);
        cardSliderViewPager.setAdapter(new CardAdapter());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.getProductList();
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void onNext(List<Product> productList) {
        itemsAdapter =new ItemsAdapter(this,productList);
        recycler_view.setAdapter(itemsAdapter);
        itemsAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onShowDetail(Product product, SellerInfo sellerInfo) {

    }

    @Override
    public void onAttemptPurchase(Boolean status, String message) {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void onItemClick(String id) {
        Intent intent = new Intent(PurchaseProductActivity.this,ProductDetailActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
    }
}
