package com.atos.fishnet.purchase.view;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.sell.model.Product;
import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.CardViewHolder> {

    private Context mContext;
    private List<Product> mProductList;
    private ItemsAdapter.ItemClickListener mCallback;

    public ItemsAdapter(Context context, List<Product> productList){
        this.mContext= context;
        this.mProductList = productList;
    }

    @Override
    public ItemsAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_top_selling, parent, false);
        return new ItemsAdapter.CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsAdapter.CardViewHolder holder, int position) {
        Product product = mProductList.get(position);

        if(product.getImage_file()!= null)
            Glide
                .with(mContext)
                .load(Uri.parse(product.getImage_file()))
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.bar_curve_white)
                .into(holder.image_view);
        holder.title.setText(product.getTitle());

        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatCurrency = new DecimalFormatSymbols();
        formatCurrency.setCurrencySymbol("RM ");
        formatCurrency.setMonetaryDecimalSeparator('.');
        formatCurrency.setGroupingSeparator(',');
        df.setDecimalFormatSymbols(formatCurrency);

        holder.subtitle.setText(product.getWeight() +"KG" + " | "+ df.format(product.getCurrent_price()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null)
                    mCallback.onItemClick(product.get_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view)
        ImageView image_view;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subtitle)
        TextView subtitle;
        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void setOnItemClickListener(final ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClick(String id);
    }
}
