package com.atos.fishnet.purchase.view;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.eWallet.view.InputAmountActivity;
import com.atos.fishnet.eWallet.view.PaymentPageActivity;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.atos.fishnet.purchase.model.SellerInfo;
import com.atos.fishnet.purchase.presenter.PurchaseProductPresenterImpl;
import com.atos.fishnet.sell.model.Product;
import com.bumptech.glide.Glide;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProductDetailActivity extends BaseActivity implements PurchaseProductPresenterImpl.View {

    @BindView(R.id.image_view)
    ImageView image_view;
    @BindView(R.id.current_price)
    TextView current_price;
    @BindView(R.id.txt_weight)
    TextView txt_weight;
    @BindView(R.id.product_title)
    TextView product_title;
    @BindView(R.id.btn_purchase)
    Button btn_purchase;
    @BindView(R.id.progress_layout)
    RelativeLayout progress_layout;

    Handler mHandler = new Handler(Looper.getMainLooper());

    private String id;
    private PurchaseProductPresenterImpl mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        mPresenter = new PurchaseProductPresenterImpl(this, this);
        id = getIntent().getExtras().getString("id");
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.getProductDetails(id);
    }

    public void onShowDetail(Product product, SellerInfo sellerInfo){
        Glide
                .with(this)
                .load(Uri.parse(product.getImage_file()))
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.bar_curve_white)
                .into(image_view);

        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatCurrency = new DecimalFormatSymbols();
        formatCurrency.setCurrencySymbol("RM ");
        formatCurrency.setMonetaryDecimalSeparator('.');
        formatCurrency.setGroupingSeparator(',');
        df.setDecimalFormatSymbols(formatCurrency);

        current_price.setText(df.format(product.getCurrent_price()));
        txt_weight.setText(product.getWeight()+"KG");
        product_title.setText(product.getTitle());

        btn_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FishnetStorage.getInstance().getIsCreditCardAdded()) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(ProductDetailActivity.this, SweetAlertDialog.NORMAL_TYPE);

                    pDialog.setContentText("Do you want to purchase " + product.getTitle() + " ?");
                    pDialog.showCancelButton(true);

                    pDialog.setTitleText(String.valueOf("Purchase Confirmation"));
                    pDialog.setCancelText("Cancel");
                    pDialog.setConfirmText("Confirm");
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sDialog) {
                            mPresenter.purchaseProduct(String.valueOf(product.getCurrent_price()), sellerInfo.getSeller_id(), id);
                            sDialog.cancel();
                        }
                    });
                    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    pDialog.show();

                   /* pDialog.getButton(SweetAlertDialog.BUTTON_CONFIRM).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    pDialog.getButton(SweetAlertDialog.BUTTON_CONFIRM).setTextColor(getResources().getColor(R.color.White));
                    pDialog.getButton(SweetAlertDialog.BUTTON_CANCEL).setBackgroundColor(getResources().getColor(R.color.red));
                    pDialog.getButton(SweetAlertDialog.BUTTON_CANCEL).setTextColor(getResources().getColor(R.color.White));*/
                }
                else
                    startActivity(new Intent(ProductDetailActivity.this, PaymentPageActivity.class));
            }
        });
    }

    @Override
    public void onAttemptPurchase(Boolean status, String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        if(status)
            finish();
        else
            startActivity(new Intent(ProductDetailActivity.this, InputAmountActivity.class));
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.VISIBLE));
    }

    @Override
    public void dismissProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.GONE));
    }


    @Override
    public void onNext(List<Product> productList) {

    }
}
