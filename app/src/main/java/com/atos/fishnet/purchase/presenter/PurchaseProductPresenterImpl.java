package com.atos.fishnet.purchase.presenter;

import android.content.Context;
import android.net.Uri;
import android.view.View;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.purchase.model.SellerInfo;
import com.atos.fishnet.sell.model.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PurchaseProductPresenterImpl {
    private APIInterface mApiInterface;
    private View mView;
    private Context mContext;

    public PurchaseProductPresenterImpl(Context context, View view){
        this.mView = view;
        this.mContext = context;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    public void getProductList(){
        mView.showProgress();
        mApiInterface.getProductList(FishnetStorage.getInstance().getCurrentUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        mView.onError(e.getMessage());

                    }

                    @Override
                    public void onNext(Root root) {
                        mView.dismissProgress();

                        if (!root.getStatus())
                            mView.onError(root.getMessage());
                         else
                            mView.onNext(root.getProduct_list());
                    }
                });
    }

    public void getProductDetails(String id) {
        mView.showProgress();

            mApiInterface.getProductDetails(id).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Root>() {
                        @Override
                        public void onCompleted() {
                            mView.showComplete();
                        }

                        @Override
                        public void onError(Throwable e) {
                            //productDetailsActivity.hideProgress();
                            mView.onError(e.getMessage());
                        }

                        @Override
                        public void onNext(Root status) {
                            mView.dismissProgress();
                            if (!status.getStatus())
                                mView.onError(status.getMessage());
                             else
                                mView.onShowDetail(status.getProduct(), status.getSellerInfo());
                        }
                    });

    }

    public void purchaseProduct(String price, String sellerID, String productID){
        mView.showProgress();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", FishnetStorage.getInstance().getCurrentUserId());
            jsonObject.put("price", price);
            jsonObject.put("seller", sellerID);
            jsonObject.put("product_id", productID);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body =
                RequestBody.create(MediaType.parse("raw"), jsonObject.toString());

        mApiInterface.purchaseProduct(FishnetStorage.getInstance().getAuthToken(),body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        mView.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(Root status) {
                        mView.dismissProgress();
                        mView.onAttemptPurchase(status.getStatus(), status.getMessage());
                        FishnetStorage.getInstance().setWalletBalance(status.getBalance());
                    }
                });
    }


    public interface View {

        void onError(String error);

        void showComplete();

        void showProgress();

        void onNext(List<Product> productList);

        void onShowDetail(Product product, SellerInfo sellerInfo);

        void onAttemptPurchase(Boolean status,String message);

        void dismissProgress();

    }
}
