package com.atos.fishnet.purchase.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.github.islamkhsh.CardSliderAdapter;

import org.jetbrains.annotations.NotNull;

public class CardAdapter  extends CardSliderAdapter<CardAdapter.CardViewHolder> {


    @Override
    public int getItemCount(){
        return 5;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_page, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void bindVH(@NotNull CardViewHolder cardViewHolder, int i) {

    }

    class CardViewHolder extends RecyclerView.ViewHolder {

        public CardViewHolder(View view){
            super(view);
        }
    }
}