package com.atos.fishnet.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.fishnet.view.MainActivity;
import com.atos.fishnet.login.presenter.AuthActivityContract;
import com.atos.fishnet.login.presenter.AuthPresenterImpl;
import com.atos.fishnet.onboarding.view.OnboardingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements AuthActivityContract.View {
    AuthPresenterImpl mAuthPresenter;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.txt_sign_up)
    TextView txtSignUp;

    @BindView(R.id.input_login_email)
    EditText txtEmail;

    @BindView(R.id.input_login_password)
    EditText txtPassword;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuthPresenter = new AuthPresenterImpl(this);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CannotBeEmpty()) {
                    if (ValidateEmail(txtEmail.getText()))
                        SaveLoginDetails();
                    else
                        Toast.makeText(getApplicationContext(), "Invalid Email Format", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), "Email Address or Password cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }

    public boolean ValidateEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean CannotBeEmpty() {
        return (!TextUtils.isEmpty(txtEmail.getText()) && !TextUtils.isEmpty(txtPassword.getText()));
    }

    public void SaveLoginDetails() {
        Log.d("Tag1","entering layer 1");
        Log.d("Tag2",txtEmail.getText().toString());
        mAuthPresenter.onFetchLoginDetails(txtEmail.getText().toString(), txtPassword.getText().toString());
    }

    @Override
    public void onNext(String msg, Boolean status) {

        if (status) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            if (!FishnetStorage.getInstance().isFirstTimeUser()) {
                startActivity(new Intent(LoginActivity.this, OnboardingActivity.class));
            } else {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        } else {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }}

    @Override
    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
