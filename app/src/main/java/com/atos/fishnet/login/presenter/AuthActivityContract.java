package com.atos.fishnet.login.presenter;

public interface AuthActivityContract {

    interface Presenter{
        void onFetchRegisterDetails(String username, String password, String email);
        void onFetchLoginDetails(String email, String password);
    }

    interface View {
        void onNext(String msg, Boolean status);
        void showError(String msg);
    }
}
