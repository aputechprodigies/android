package com.atos.fishnet.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.MainActivity;
import com.atos.fishnet.login.presenter.AuthActivityContract;
import com.atos.fishnet.login.presenter.AuthPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity implements AuthActivityContract.View {

    @BindView(R.id.btn_register)
    Button btnRegister;

    @BindView(R.id.input_username)
    EditText txtUsername;

    @BindView(R.id.input_email)
    EditText txtEmail;

    @BindView(R.id.input_password)
    EditText txtPassword;

    @BindView(R.id.txt_sign_in)
    TextView txtSignIn;

    AuthPresenterImpl mAuthPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mAuthPresenter = new AuthPresenterImpl(this);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CannotBeEmpty()) {
                        if (validateEmail(txtEmail.getText()))
                            saveDetails();
                        else
                            Toast.makeText(getApplicationContext(), "Incorrect Email Address", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), "Text fields cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });

       txtSignIn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });
    }

    public void saveDetails() {
        mAuthPresenter.onFetchRegisterDetails(txtUsername.getText().toString(), txtPassword.getText().toString(), txtEmail.getText().toString());
    }

    public boolean validateEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public Boolean CannotBeEmpty() {
        return (!TextUtils.isEmpty(txtUsername.getText()) && !TextUtils.isEmpty(txtEmail.getText()) && !TextUtils.isEmpty(txtPassword.getText()));
    }


    @Override
    public void onNext(String msg, Boolean status) {
        if (!status) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
