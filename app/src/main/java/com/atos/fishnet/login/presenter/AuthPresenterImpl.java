package com.atos.fishnet.login.presenter;

import android.util.Log;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.login.model.User;
import com.atos.fishnet.login.view.LoginActivity;
import com.atos.fishnet.login.view.RegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthPresenterImpl implements AuthActivityContract.Presenter {

    private RegisterActivity registerActivity;
    private LoginActivity loginActivity;
    private APIInterface mAPIInterface;

    public AuthPresenterImpl(RegisterActivity view) {
        this.registerActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public AuthPresenterImpl(LoginActivity view) {
        this.loginActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    private void setAuthToken(String authToken) {
        FishnetStorage.getInstance().setAuthToken(authToken);
    }

    private void storeUserBasicInfo(final User user) {
        FishnetStorage.getInstance().setCurrentUserId(user.getId()); // store user id
        FishnetStorage.getInstance().setCurrentUserName(user.getName());// store user name
        FishnetStorage.getInstance().setWalletBalance(user.getBalance());
    }

    @Override
    public void onFetchRegisterDetails(String username, String password, String email) {
        JSONObject object = new JSONObject();
        try {
            object.put("name", username);
            object.put("password", password);
            object.put("email", email);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"),object.toString());
       mAPIInterface.signUpByEmail(body).subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(new Subscriber<Root>(){

                   @Override
                   public void onCompleted() {

                   }

                   @Override
                   public void onError(Throwable e) {
                       System.out.println(e.toString());
                   }

                   @Override
                   public void onNext(Root root) {
                       registerActivity.onNext(root.getMessage(),root.getStatus());
                   }
               });

    }

    @Override
    public void onFetchLoginDetails(String email, String password) {
        Log.d("Tag3",email);
        JSONObject object = new JSONObject();
        try {
            object.put("password", password);
            object.put("email", email);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"),object.toString());
        mAPIInterface.signInByEmail(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>(){

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        loginActivity.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(Root root) {
                        if(root.getStatus()) {
                            storeUserBasicInfo(root.getUser());
                            FishnetStorage.getInstance().setLoggedIn(true);
                            setAuthToken(root.getToken());
                        }
                        loginActivity.onNext(root.getMessage(),root.getStatus());
                    }
                });

    }
}
