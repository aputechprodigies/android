package com.atos.fishnet.login.model;

import com.google.gson.annotations.SerializedName;

public class DeviceInfo {

    @SerializedName("device_identifier")

    private String deviceIdentifier;
    @SerializedName("device_information")

    private String deviceInformation;
    @SerializedName("device_type")

    private String deviceType;

    public Object getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public String getDeviceInformation() {
        return deviceInformation;
    }

    public void setDeviceInformation(String deviceInformation) {
        this.deviceInformation = deviceInformation;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
