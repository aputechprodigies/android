package com.atos.fishnet.model;

import com.atos.fishnet.eWallet.model.Card;
import com.atos.fishnet.home.model.WeatherDetails;
import com.atos.fishnet.knowledgebase.model.Info;
import com.atos.fishnet.login.model.User;
import com.atos.fishnet.notification.model.Notification;
import com.atos.fishnet.purchase.model.SellerInfo;
import com.atos.fishnet.sell.model.Product;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Root {

    @SerializedName("status")
    private Boolean status;
    @SerializedName("message")
    private String message;
    @SerializedName("user")
    private User user;
    @SerializedName("token")
    private String token;
    @SerializedName("location")
    private String location;
    @SerializedName("index")
    private String index;
    @SerializedName("fish")
    private String fish;
    @SerializedName("data")
    private String data;
    @SerializedName("url")
    private String url;
    @SerializedName("product")
    private Product product;
    @SerializedName("product_list")
    private List<Product> product_list;
    @SerializedName("info")
    private List<Info> info;
    @SerializedName("card")
    public Card card;
    @SerializedName("seller_info")
    private SellerInfo sellerInfo;
    @SerializedName("balance")
    private String balance;
    @SerializedName("notifications")
    public List<Notification> notifications;
    @SerializedName("weatherDetails")
    private WeatherDetails weatherDetails;

    public WeatherDetails getWeatherDetails() {
        return weatherDetails;
    }

    public void setWeatherDetails(WeatherDetails weatherDetails) {
        this.weatherDetails = weatherDetails;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public SellerInfo getSellerInfo() {
        return sellerInfo;
    }

    public void setSellerInfo(SellerInfo sellerInfo) {
        this.sellerInfo = sellerInfo;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public List<Info> getInfo() {
        return info;
    }

    public void setInfo(List<Info> info) {
        this.info = info;
    }

    public List<Product> getProduct_list() {
        return product_list;
    }

    public void setProduct_list(List<Product> product_list) {
        this.product_list = product_list;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getFish() {
        return fish;
    }

    public void setFish(String fish) {
        this.fish = fish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
