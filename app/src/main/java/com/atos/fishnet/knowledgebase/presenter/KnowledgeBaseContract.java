package com.atos.fishnet.knowledgebase.presenter;

import com.atos.fishnet.knowledgebase.model.Info;

import java.util.List;

public interface KnowledgeBaseContract {

    interface View {
        void onNextRed(String msg, Boolean status, List<Info> info);
        void onNextOrange(String msg, Boolean status, List<Info> info);
        void onNextYellow(String msg, Boolean status, List<Info> info);
        void showError(String msg);
    }

    interface Presenter {
        void onFetchFishDetails(String spectrum);
    }
}
