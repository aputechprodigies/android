package com.atos.fishnet.knowledgebase.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.atos.fishnet.chatbot.activities.ImageFFActivity;
import com.atos.fishnet.fishnet.utils.RoundRectCornerImageView;
import com.bumptech.glide.Glide;

import java.util.List;

public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.CardViewHolder> {

    private List<Info> mFishInfo;
    private Context context;

    public InfoAdapter(List<Info> fishInfo, Context context) {
        this.mFishInfo = fishInfo;
        this.context = context;
    }

    @NonNull
    @Override
    public InfoAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_fish_info, parent, false);
        return new InfoAdapter.CardViewHolder(view);
    }

    public static String capitalizeWord(String str){
        String words[]=str.split("\\s");
        String capitalizeWord="";
        for(String w:words){
            String first=w.substring(0,1);
            String afterfirst=w.substring(1);
            capitalizeWord+=first.toUpperCase()+afterfirst+" ";
        }
        return capitalizeWord.trim();
    }
    @Override
    public void onBindViewHolder(InfoAdapter.CardViewHolder holder, int position) {
        Info fishInfo = mFishInfo.get(position);
        String correctFishName= capitalizeWord(fishInfo.getName());
      holder.fishName.setText(correctFishName);

        if (fishInfo.getLocation() != null) {
            Glide
                    .with(context)
                    .load(Uri.parse(fishInfo.getLocation()))
                    .centerInside()
                    .thumbnail(0.1f)
                    .into(holder.fishImage);
        }

        holder.fishImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageFFActivity.class);
                intent.putExtra("photoURI",fishInfo.getLocation());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFishInfo.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        private TextView fishName;
        private RoundRectCornerImageView fishImage;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            fishName = itemView.findViewById(R.id.txt_fish_name);
            fishImage = itemView.findViewById(R.id.image_view_fish);
        }
    }
}
