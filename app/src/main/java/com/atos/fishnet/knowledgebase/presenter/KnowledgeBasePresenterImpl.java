package com.atos.fishnet.knowledgebase.presenter;

import android.util.Log;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.knowledgebase.view.KnowledgeBaseActivity;
import com.atos.fishnet.model.Root;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class KnowledgeBasePresenterImpl implements KnowledgeBaseContract.Presenter {

    private KnowledgeBaseActivity knowledgeBaseActivity;
    private APIInterface mAPIInterface;

    public KnowledgeBasePresenterImpl(KnowledgeBaseActivity view) {
        this.knowledgeBaseActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    @Override
    public void onFetchFishDetails(String spectrum) {
        mAPIInterface.getFishInfo(FishnetStorage.getInstance().getAuthToken(), spectrum).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        knowledgeBaseActivity.showError(e.toString());
                    }

                    @Override
                    public void onNext(Root root) {
                        if(spectrum.equals("Red"))
                            knowledgeBaseActivity.onNextRed(root.getMessage(), root.getStatus(), root.getInfo());
                        else if(spectrum.equals("Orange"))
                            knowledgeBaseActivity.onNextOrange(root.getMessage(), root.getStatus(), root.getInfo());
                        if(spectrum.equals("Yellow"))
                            knowledgeBaseActivity.onNextYellow(root.getMessage(), root.getStatus(), root.getInfo());
                    }
                });
    }
}
