package com.atos.fishnet.knowledgebase.view;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.atos.fishnet.knowledgebase.model.Info;
import com.atos.fishnet.knowledgebase.model.InfoAdapter;
import com.atos.fishnet.knowledgebase.presenter.KnowledgeBaseContract;
import com.atos.fishnet.knowledgebase.presenter.KnowledgeBasePresenterImpl;
import com.atos.fishnet.login.presenter.AuthPresenterImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KnowledgeBaseActivity extends BaseActivity implements KnowledgeBaseContract.View {
    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.spectrum_red)
    RecyclerView spectrum_red;
    @BindView(R.id.spectrum_yellow)
    RecyclerView spectrum_yellow;
    @BindView(R.id.spectrum_orange)
    RecyclerView spectrum_orange;

    InfoAdapter infoAdapterYellow;
    InfoAdapter infoAdapterRed;
    InfoAdapter infoAdapterOrange;
    KnowledgeBasePresenterImpl knowledgeBasePresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge_base);
        ButterKnife.bind(this);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        knowledgeBasePresenter = new KnowledgeBasePresenterImpl(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        knowledgeBasePresenter.onFetchFishDetails("Yellow");
        knowledgeBasePresenter.onFetchFishDetails("Orange");
        knowledgeBasePresenter.onFetchFishDetails("Red");
    }

    @Override
    public void onNextRed(String msg, Boolean status, List<Info> info) {
        Log.d("ABC","EnteringABC1");
        infoAdapterRed = new InfoAdapter(info,this);
        spectrum_red.setAdapter(infoAdapterRed);
    }

    @Override
    public void onNextOrange(String msg, Boolean status, List<Info> info) {
        Log.d("ABC","EnteringABC2");
        infoAdapterOrange = new InfoAdapter(info,this);
        spectrum_orange.setAdapter(infoAdapterOrange);
    }

    @Override
    public void onNextYellow(String msg, Boolean status, List<Info> info) {
        Log.d("ABC","EnteringABC3");
        infoAdapterYellow = new InfoAdapter(info,this);
        spectrum_yellow.setAdapter(infoAdapterYellow);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
}
