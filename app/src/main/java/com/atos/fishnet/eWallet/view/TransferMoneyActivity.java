package com.atos.fishnet.eWallet.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.eWallet.helper.DecimalDigitsInputFilter;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.ref.WeakReference;

import me.abhinay.input.CurrencyEditText;

import static me.abhinay.input.CurrencySymbols.MALAYSIA;

public class TransferMoneyActivity extends AppCompatActivity {

    CurrencyEditText txtTransfer;
    TextInputEditText purposeTxt;
    ImageView imgBack;
    Button confirm;
    double amount;
    public static String money;
    public static String desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_money);
        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtTransfer = findViewById(R.id.txtTransfer);
        txtTransfer.setCurrency(MALAYSIA);
        txtTransfer.setDelimiter(false);
        txtTransfer.setSpacing(false);
        txtTransfer.setDecimals(false);
        txtTransfer.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2)});
        txtTransfer.addTextChangedListener(new MoneyTextWatcher(txtTransfer));

        txtTransfer.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        txtTransfer.setLongClickable(false);
        txtTransfer.setTextIsSelectable(false);

        confirm = findViewById(R.id.confirm);
        confirm.setEnabled(false);
        confirm.setAlpha(.5f);

        purposeTxt = findViewById(R.id.purposeTxt);
        purposeTxt.addTextChangedListener(new purp(purposeTxt));

        //get balance from ui and set to amount_string
        String amount_string = FishnetStorage.getInstance().getWalletBalance();
        amount = Double.parseDouble(amount_string);

    }

    public void onConfirm(View view) {
        money = txtTransfer.getText().toString();
        desc = purposeTxt.getText().toString();
        Intent i = new Intent(getApplicationContext(), QRScannerTransfer.class);
        i.putExtra("money", money);
        i.putExtra("desc", desc);
        startActivity(i);
        finish();
    }

    public class MoneyTextWatcher implements TextWatcher {
        private final WeakReference<CurrencyEditText> editTextWeakReference;

        public MoneyTextWatcher(CurrencyEditText editText) {
            editTextWeakReference = new WeakReference<CurrencyEditText>(editText);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {

            CurrencyEditText editText = editTextWeakReference.get();
            int empty = 0;
            int flagva = 0;
            char first = '1';

            if (!editText.getText().toString().equals("")) {
                first = editText.getText().toString().charAt(0);
            }

            if (editText.getText().toString().equals(".") || first == '0') {
                editText.setText("");
                return;
            }

            if (!editText.getText().toString().equals("")) {
                Double h = editText.getCleanDoubleValue();

                if (h > amount) {
                    Toast.makeText(getApplicationContext(), "Insufficient", Toast.LENGTH_SHORT).show();
                    confirm.setEnabled(false);
                    confirm.setAlpha(.5f);
                    flagva = 1;

                } else {
                    confirm.setEnabled(true);
                    confirm.setAlpha(1.0f);
                }

            } else {
                confirm.setEnabled(false);
                confirm.setAlpha(.5f);
                empty = 1;
            }


            if (purposeTxt.getText().toString().equals("") || flagva == 1 || empty == 1) {
                confirm.setEnabled(false);
                confirm.setAlpha(.5f);
            } else {
                confirm.setEnabled(true);
                confirm.setAlpha(1.0f);
            }


        }
    }


    public class purp implements TextWatcher {

        private final WeakReference<TextInputEditText> editTextWeakReference;

        public purp(TextInputEditText purposeTxt) {
            editTextWeakReference = new WeakReference<TextInputEditText>(purposeTxt);
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            Double amo;
            int flagva = 0;

            if (!txtTransfer.getText().toString().equals("")) {
                amo = txtTransfer.getCleanDoubleValue();
            } else {
                amo = 0.00;
            }

            TextInputEditText editText = editTextWeakReference.get();

            if (editText.getText().toString().equals("")) {
                confirm.setEnabled(false);
                confirm.setAlpha(0.5f);
                flagva = 1;
            } else {
                confirm.setEnabled(true);
                confirm.setAlpha(1.0f);
            }

            if (txtTransfer.getText().toString().equals("") || amo > amount || flagva == 1) {
                confirm.setEnabled(false);
                confirm.setAlpha(0.5f);
            } else {
                confirm.setEnabled(true);
                confirm.setAlpha(1.0f);
            }
        }

    }
}
