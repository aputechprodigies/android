package com.atos.fishnet.eWallet.view;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.eWallet.presenter.EWalletPresenterImpl;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.zxing.Result;

import java.net.URISyntaxException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class QRScannerTransfer extends AppCompatActivity implements ZXingScannerView.ResultHandler  {

    ZXingScannerView scannerView;
    EWalletPresenterImpl eWalletPresenter;
    public static Socket socket;
    private static final int REQUEST_CAMERA = 1;
    String money, balance, sender, qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);

        eWalletPresenter=new EWalletPresenterImpl(this);

        try {
            socket = IO.socket("http://52.76.35.182:3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        //initialize socket connection
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        });

        socket.connect();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                Toast.makeText(QRScannerTransfer.this, "Scan the Fisherman's QR code!", Toast.LENGTH_LONG).show();
            } else {
                requestPermission();
            }
        }

        Intent i = getIntent();
        Bundle value = i.getExtras();
        money=value.getString("money");
        money=money.replace("RM","");
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(QRScannerTransfer.this, CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            }
        }
    }

    public  void complete(String status){
        Toast.makeText(getApplicationContext(),""+status,Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        final String qr = result.getText();
        eWalletPresenter.onScanQR(money, FishnetStorage.getInstance().getCurrentUserId(),qr);

    }
}
