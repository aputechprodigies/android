package com.atos.fishnet.eWallet.view;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.eWallet.presenter.EWalletActivityContract;
import com.atos.fishnet.eWallet.presenter.EWalletPresenterImpl;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class QRActivity extends AppCompatActivity implements EWalletActivityContract.View {

    ImageView QRimage;
    ImageView img_back;
    public static Socket socket;
    TextView txt_balance;
    EWalletPresenterImpl eWalletPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        QRimage = findViewById(R.id.QRimage);
        eWalletPresenter=new EWalletPresenterImpl(this);
        img_back = findViewById(R.id.img_back);
        txt_balance = findViewById(R.id.txt_balance);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            socket = IO.socket("http://52.76.35.182:3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatCurrency = new DecimalFormatSymbols();

        formatCurrency.setCurrencySymbol("MYR ");
        formatCurrency.setMonetaryDecimalSeparator('.');
        formatCurrency.setGroupingSeparator(',');

        df.setDecimalFormatSymbols(formatCurrency);
        txt_balance.setText(df.format(Double.valueOf(FishnetStorage.getInstance().getWalletBalance())));


        //initialize socket connection
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(QRActivity.this, "Scan to transfer", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        socket.connect();
        socket_events();

        eWalletPresenter.onGetQR(FishnetStorage.getInstance().getCurrentUserId());

    }


    // qr is set to image view
    public void qr_generate(String qr_id) {

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode("" + qr_id, BarcodeFormat.QR_CODE, 250, 250);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            QRimage.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private void socket_events() {

        socket.on("status" + FishnetStorage.getInstance().getCurrentUserId(), new Emitter.Listener() {
            @Override
            public void call(final Object... args) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (args[0].toString().equals(("Session has exprired from receiver side."))) {
                            Toast.makeText(QRActivity.this, "Session has exprired from receiver side.", Toast.LENGTH_SHORT).show();
                        } else if (args[0].toString().equals("Successfully transfered") || args[0].toString().equals("Successfully received")) {

                            // args[1].toString(); can be used for get ewallet balance after receiving
                            Toast.makeText(QRActivity.this, "Successfully received", Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(QRActivity.this, "" + args[0], Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });


        socket.on("approval" + FishnetStorage.getInstance().getCurrentUserId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                final String sender = args[0].toString();
                final String money = args[1].toString();
                final String sender_id = args[2].toString();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final JSONObject obj1 = new JSONObject();
                        try {
                            obj1.put("status", "accept");
                            obj1.put("sender_id", sender_id);
                            obj1.put("receiver", FishnetStorage.getInstance().getCurrentUserId());
                            obj1.put("money", money);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final JSONObject obj2 = new JSONObject();
                        try {
                            obj2.put("status", "reject");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        AlertDialog.Builder build = new AlertDialog.Builder(QRActivity.this);

                        build.setTitle("Transfer").setMessage("" + sender + " wants to send you RM" + money)
                                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        socket.emit("status", obj1);

                                    }
                                })
                                .setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        socket.emit("status", obj2);
                                    }
                                });
                        AlertDialog alert = build.create();
                        alert.show();
                    }
                });

            }
        });
    }


    @Override
    public void onBackPressed() {
        socket.emit("disc", "h");
        socket.disconnect();
        finish();
    }

    @Override
    public void onNext(String status) {


    }

    @Override
    public void showError(String msg) {

    }

}
