package com.atos.fishnet.eWallet.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.BinderThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.barchart.BarChart;
import com.atos.fishnet.barchart.BarChartModel;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.fishnet.view.MainFragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class EWalletFragment extends MainFragment {

    private Unbinder mUnBinder;
    @BindView(R.id.top_up_container)
    LinearLayout top_up_container;
    @BindView(R.id.pay_container)
    LinearLayout pay_container;
    @BindView(R.id.request_container)
    LinearLayout request_container;
    @BindView(R.id.bar_chart_vertical)
    BarChart barChart;
    @BindView(R.id.txt_balance)
    TextView txt_balance;
    @BindView(R.id.btn_card)
    ImageView btn_card;

    public static EWalletFragment getInstance(){
        return new EWalletFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_ewallet, container, false);
        mUnBinder = ButterKnife.bind(this, view);

        top_up_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FishnetStorage.getInstance().getIsCreditCardAdded())
                    startActivity(new Intent(EWalletFragment.this.getActivity(),InputAmountActivity.class));
                else
                    startActivity(new Intent(EWalletFragment.this.getActivity(),PaymentPageActivity.class));
            }
        });

        pay_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EWalletFragment.this.getActivity(),TransferMoneyActivity.class));
            }
        });

        request_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EWalletFragment.this.getActivity(),QRActivity.class));
            }
        });

        btn_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EWalletFragment.this.getActivity(),PaymentPageActivity.class));
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatCurrency = new DecimalFormatSymbols();

        formatCurrency.setCurrencySymbol("RM ");
        formatCurrency.setMonetaryDecimalSeparator('.');
        formatCurrency.setGroupingSeparator(',');

        df.setDecimalFormatSymbols(formatCurrency);
        txt_balance.setText(df.format(Double.valueOf(FishnetStorage.getInstance().getWalletBalance())));

        barChart.setBarMaxValue(120);

        List<BarChartModel> barChartModelList = new ArrayList<>();

        for(int i =0;i<7;i++){
            BarChartModel barChartModel = new BarChartModel();
            switch (i) {
                case 0:
                    barChartModel.setBarText("Mon");
                    barChartModel.setBarValue(90);

                    break;
                case 1:
                    barChartModel.setBarText("Tue");
                    barChartModel.setBarValue(80);

                    break;
                case 2:
                    barChartModel.setBarText("Wed");
                    barChartModel.setBarValue(88);

                    break;
                case 3:
                    barChartModel.setBarText("Thu");
                    barChartModel.setBarValue(60);

                    break;
                case 4:
                    barChartModel.setBarText("Fri");
                    barChartModel.setBarValue(50);

                    break;
                case 5:
                    barChartModel.setBarText("Sat");
                    barChartModel.setBarValue(0);

                    break;
                case 6 :
                    barChartModel.setBarText("Sun");
                    barChartModel.setBarValue(75);

                    break;
            }

            barChartModelList.add(barChartModel);
        }
        barChart.addBar(barChartModelList);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }
}
