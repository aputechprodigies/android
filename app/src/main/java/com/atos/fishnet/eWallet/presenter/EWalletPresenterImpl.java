package com.atos.fishnet.eWallet.presenter;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.eWallet.model.EwalletModel;
import com.atos.fishnet.eWallet.view.InputAmountActivity;
import com.atos.fishnet.eWallet.view.QRActivity;
import com.atos.fishnet.eWallet.view.QRScannerTransfer;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EWalletPresenterImpl implements EWalletActivityContract.Presenter {

    private QRScannerTransfer qrScannerTransfer;
    private QRActivity qrActivity;
    private InputAmountActivity inputAmountActivity;
    private APIInterface mAPIInterface;

    public EWalletPresenterImpl(QRActivity view) {
        this.qrActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public EWalletPresenterImpl(InputAmountActivity view) {
        this.inputAmountActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public EWalletPresenterImpl(QRScannerTransfer view) {
        this.qrScannerTransfer = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    @Override
    public void onGetQR(String id) {

        mAPIInterface.get_qr(FishnetStorage.getInstance().getAuthToken(), id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EwalletModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(EwalletModel ewalletModel) {
                        qrActivity.qr_generate(ewalletModel.getQr_id());
                    }

                });
    }

    @Override
    public void onScanQR(String money, String sender, String qr) {

        JSONObject object = new JSONObject();
        try {
            object.put("money", money);
            object.put("sender", sender);
            object.put("qr", qr);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.scan_qr(FishnetStorage.getInstance().getAuthToken(), body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EwalletModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        qrActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(EwalletModel ewalletModel) {
                        qrScannerTransfer.complete(ewalletModel.getStatus());
                    }

                });
    }

    @Override
    public void onTopUpWithCard(String id, Double money) {
        JSONObject object = new JSONObject();
        try {
            object.put("id", id);
            object.put("money", money);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.topup(FishnetStorage.getInstance().getAuthToken(), body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EwalletModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        inputAmountActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(EwalletModel ewalletModel) {
                        inputAmountActivity.onNext(ewalletModel.getStatus());
                        inputAmountActivity.onBalanceChanged(ewalletModel.getBalance());
                    }

                });
    }
}
