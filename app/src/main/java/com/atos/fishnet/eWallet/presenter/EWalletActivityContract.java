package com.atos.fishnet.eWallet.presenter;

public interface EWalletActivityContract {

    interface Presenter {
        void onGetQR(String id);
        void onScanQR(String money, String sender, String qr);
        void onTopUpWithCard(String id, Double money);
    }

    interface View {
        void onNext(String status);
        void showError(String msg);
    }
}
