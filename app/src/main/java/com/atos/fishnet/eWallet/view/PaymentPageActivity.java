package com.atos.fishnet.eWallet.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.component.ApplicationComponent;
import com.atos.fishnet.eWallet.di.component.DaggerPaymentComponent;
import com.atos.fishnet.eWallet.di.component.PaymentComponent;
import com.atos.fishnet.eWallet.di.module.PaymentContextModule;
import com.atos.fishnet.eWallet.di.module.PaymentPresenterModule;
import com.atos.fishnet.eWallet.model.Card;
import com.atos.fishnet.eWallet.presenter.PaymentContract;
import com.atos.fishnet.eWallet.presenter.PaymentPresenterImpl;
import com.atos.fishnet.fishnet.FishnetApplication;
import com.atos.fishnet.fishnet.view.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentPageActivity extends BaseActivity implements PaymentContract.View {

    @BindView(R.id.toolbar_title_tv)
    TextView mToolbarTitleTv;
    @BindView(R.id.toolbar_left_btn)
    ImageView mBackBtn;
    @BindView(R.id.add_credit_debit)
    RelativeLayout add_credit_debit;
    @BindView(R.id.progress_layout)
    RelativeLayout progress_layout;
    @BindView(R.id.no_avail_card)
    RelativeLayout no_avail_card;
    @BindView(R.id.avail_card)
    LinearLayout avail_card;
    @BindView(R.id.delete_card)
    Button delete_card;

    @BindView(R.id.card_type)
    ImageView card_type;
    @BindView(R.id.tv_card_number)
    TextView tv_card_number;
    @BindView(R.id.tv_validity)
    TextView tv_validity;

    @Inject
    PaymentPresenterImpl mPresenter;

    Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_page);
        ButterKnife.bind(this);
        setToolbar();

        ApplicationComponent applicationComponent = FishnetApplication.get(this).getAppComponent();
        PaymentComponent paymentComponent = DaggerPaymentComponent.builder()
                .paymentContextModule(new PaymentContextModule(this))
                .paymentPresenterModule(new PaymentPresenterModule(this))
                .applicationComponent(applicationComponent)
                .build();

        paymentComponent.injectPaymentPageActivity(this);

        add_credit_debit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(PaymentPageActivity.this, PaymentCardActivity.class));

            }
        });

        delete_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.deleteCard();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.getCard();
    }

    private void setToolbar() {
        mToolbarTitleTv.setText("Payment Method");
        mBackBtn.setVisibility(View.VISIBLE);

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onShowCard(Card card) {
        if(card != null){
            no_avail_card.setVisibility(View.GONE);
            avail_card.setVisibility(View.VISIBLE);
            if(card.getBrand().equals("Visa")){
                card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_white_visa));
            }else {
                card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_master_card));
            }

            String lastTwoDigits = "";
            tv_card_number.setText("\u25CF\u25CF\u25CF\u25CF \u25CF\u25CF\u25CF\u25CF \u25CF\u25CF\u25CF\u25CF "+card.getLast4());
            lastTwoDigits= String.valueOf(card.getExp_year()).substring(String.valueOf(card.getExp_year()).length() - 2);
            tv_validity.setText(String.valueOf(card.getExp_month())+"/"+lastTwoDigits);
            FishnetStorage.getInstance().setCreditCardAdded(true);

        }else{
            no_avail_card.setVisibility(View.VISIBLE);
            avail_card.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCardDeleted() {
        FishnetStorage.getInstance().setCreditCardAdded(false);
        Toast.makeText(this, "Card deleted", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onNext(String message) {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.GONE));
    }

}
