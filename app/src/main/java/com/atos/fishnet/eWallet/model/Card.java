package com.atos.fishnet.eWallet.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Card implements Serializable {
    @SerializedName("_id")
    public String _id ;
    @SerializedName("user_id")
    public String user_id ;
    @SerializedName("brand")
    public String brand ;
    @SerializedName("country")
    public String country ;
    @SerializedName("exp_month")
    public int exp_month ;
    @SerializedName("exp_year")
    public int exp_year ;
    @SerializedName("fingerprint")
    public String fingerprint ;
    @SerializedName("token")
    public String token ;
    @SerializedName("last4")
    public String last4 ;
    @SerializedName("created_at")
    public String created_at ;
    @SerializedName("__v")
    public int __v ;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getExp_month() {
        return exp_month;
    }

    public void setExp_month(int exp_month) {
        this.exp_month = exp_month;
    }

    public int getExp_year() {
        return exp_year;
    }

    public void setExp_year(int exp_year) {
        this.exp_year = exp_year;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }
}
