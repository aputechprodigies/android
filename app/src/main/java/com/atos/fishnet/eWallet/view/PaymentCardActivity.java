package com.atos.fishnet.eWallet.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.atos.fishnet.BuildConfig;
import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.component.ApplicationComponent;
import com.atos.fishnet.eWallet.di.component.DaggerPaymentComponent;
import com.atos.fishnet.eWallet.di.component.PaymentComponent;
import com.atos.fishnet.eWallet.di.module.PaymentContextModule;
import com.atos.fishnet.eWallet.di.module.PaymentPresenterModule;
import com.atos.fishnet.eWallet.model.Card;
import com.atos.fishnet.eWallet.presenter.PaymentContract;
import com.atos.fishnet.eWallet.presenter.PaymentPresenterImpl;
import com.atos.fishnet.eWallet.utils.CreditCardExpiryTextWatcher;
import com.atos.fishnet.eWallet.utils.CreditCardFormattingTextWatcher;
import com.atos.fishnet.fishnet.FishnetApplication;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.model.Token;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.atos.fishnet.eWallet.utils.CreditCardUtils.MASTERCARD;
import static com.atos.fishnet.eWallet.utils.CreditCardUtils.NONE;
import static com.atos.fishnet.eWallet.utils.CreditCardUtils.VISA;

public class PaymentCardActivity extends BaseActivity implements TextWatcher, PaymentContract.View {

    @BindView(R.id.toolbar_title_tv)
    TextView mToolbarTitleTv;
    @BindView(R.id.toolbar_left_btn)
    ImageView mBackBtn;
    @BindView(R.id.card_type)
    ImageView card_type;
    @BindView(R.id.tv_card_number)
    TextView tv_card_number;
    @BindView(R.id.tv_validity)
    TextView tv_validity;
    @BindView(R.id.txt_name)
    EditText txt_name;
    @BindView(R.id.txt_card_number)
    EditText txt_card_number;
    @BindView(R.id.et_card_type)
    ImageView et_card_type;
    @BindView(R.id.txt_validity)
    EditText txt_validity;
    @BindView(R.id.txt_cvv)
    EditText txt_cvv;
    @BindView(R.id.add_credit_card)
    Button add_credit_card;
    @BindView(R.id.progress_layout)
    RelativeLayout progress_layout;
    @Inject
    PaymentPresenterImpl mPresenter;

    Handler mHandler = new Handler(Looper.getMainLooper());


    private String cardNumber, cardCVV, cardValidity;
    private Stripe stripe;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_card);
        ButterKnife.bind(this);
        setToolbar();

        ApplicationComponent applicationComponent = FishnetApplication.get(this).getAppComponent();
        PaymentComponent paymentComponent = DaggerPaymentComponent.builder()
                .paymentContextModule(new PaymentContextModule(this))
                .paymentPresenterModule(new PaymentPresenterModule(this))
                .applicationComponent(applicationComponent)
                .build();

        paymentComponent.injectPaymentCardActivity(this);
        PaymentConfiguration.init(BuildConfig.STRIPE_TOKEN);
        stripe = new Stripe(this,
                PaymentConfiguration.getInstance().getPublishableKey());

        txt_card_number.addTextChangedListener(this);
        txt_validity.addTextChangedListener(this);
        txt_cvv.addTextChangedListener(this);
        txt_name.addTextChangedListener(this);

        number();
        valid();
        cvv();

    }

    private void setToolbar() {
        mToolbarTitleTv.setText("Payment Method");
        mBackBtn.setVisibility(View.VISIBLE);

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void number() {
        txt_card_number.addTextChangedListener(new CreditCardFormattingTextWatcher(txt_card_number, tv_card_number, getCardType(), new CreditCardFormattingTextWatcher.CreditCardType() {
            @Override
            public void setCardType(int type) {
                PaymentCardActivity.this.setCardType(type);
            }
        }));

        txt_card_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {

                    if (PaymentCardActivity.this != null) {
                        return true;
                    }

                }
                return false;
            }
        });
    }

    public void valid() {

        txt_validity.addTextChangedListener(new CreditCardExpiryTextWatcher(txt_validity, tv_validity));

        txt_validity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {

                    if (PaymentCardActivity.this != null) {
                        return true;
                    }

                }
                return false;
            }
        });
    }

    public void cvv() {

        txt_cvv.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {

                    InputMethodManager imm = (InputMethodManager)textView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick({R.id.add_credit_card})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.add_credit_card:
                if (checkEntries()) {
                    cardNumber = String.valueOf(txt_card_number.getText());
                    cardValidity = String.valueOf(txt_validity.getText());
                    cardCVV = String.valueOf(txt_cvv.getText());

                    String[] fn = cardValidity.split("/");
                    String maskedNumber = cardNumber.replace(" ", "");

                    onAddCard(maskedNumber,Integer.parseInt(fn[0]),Integer.parseInt("20"+fn[1]),cardCVV);
                }
                break;
        }
    }

    public void onAddCard(String cardNumber, int cardExpMonth,
                          int cardExpYear, String cardCVC) {
        final com.stripe.android.model.Card card = com.stripe.android.model.Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);
        card.validateNumber();
        card.validateCVC();
        if (!card.validateCard())
            Toast.makeText(PaymentCardActivity.this, "Invalid card", Toast.LENGTH_LONG).show();
        else
            tokenizeCard(card);
    }


    private void tokenizeCard(@NonNull com.stripe.android.model.Card card) {
        stripe.createToken(
                card,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {
                        mPresenter.uploadCard(token.getId());
                    }
                    @Override
                    public void onError(@NonNull Exception e) {
                        Toast.makeText(PaymentCardActivity.this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public boolean checkEntries() {
        cardNumber = String.valueOf(getNumber());
        cardValidity = String.valueOf(getValidity());
        cardCVV = String.valueOf(getCvv());

        if (TextUtils.isEmpty(txt_card_number.getText())) {
            Toast.makeText(PaymentCardActivity.this, "Enter Valid card number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(txt_validity.getText())) {
            Toast.makeText(PaymentCardActivity.this, "Enter correct validity", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(txt_cvv.getText()) || cardCVV.length() < 3) {
            Toast.makeText(PaymentCardActivity.this, "Enter valid security number", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            //Toast.makeText(PaymentCardActivity.this, "Your card is added", Toast.LENGTH_SHORT).show();
            return true;
        }
    }



    private void setAddButtonState() {
        if (txt_card_number.length() == 19 && txt_validity.length() == 5 && txt_cvv.length() == 3) {
            add_credit_card.setBackground(ContextCompat.getDrawable(this, R.drawable.btn_primary_dark));
            add_credit_card.setAlpha(1.0f);
            add_credit_card.setEnabled(true);
            add_credit_card.setClickable(true);
        } else {
            add_credit_card.setBackground(ContextCompat.getDrawable(this, R.drawable.btn_primary_dark));
            add_credit_card.setAlpha(0.3f);
            add_credit_card.setEnabled(false);
            add_credit_card.setClickable(false);
        }
    }

    public TextView getCvv() {
        return txt_cvv;
    }

    public TextView getNumber() {
        return tv_card_number;
    }

    public TextView getValidity() {
        return tv_validity;
    }

    public ImageView getCardType() {
        return card_type;
    }


    public void setCardType(int type) {

        switch (type) {
            case VISA:
                card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_white_visa));
                et_card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_white_visa));
                break;
            case MASTERCARD:
                card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_master_card));
                et_card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_master_card));
                break;
            case NONE:
                card_type.setImageResource(android.R.color.transparent);
                et_card_type.setImageResource(android.R.color.transparent);
                break;

        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (txt_name.getText().hashCode() == charSequence.hashCode()) {
            if (charSequence.length() == 0) {
                txt_name.requestFocus();
            } else if (txt_card_number.getText().hashCode() == charSequence.hashCode()) {
                if (charSequence.length() == 0) {
                    txt_name.requestFocus();
                }
            } else if (txt_validity.getText().hashCode() == charSequence.hashCode()) {
                if (charSequence.length() == 0) {
                    txt_card_number.requestFocus();
                }
            } else if (txt_cvv.getText().hashCode() == charSequence.hashCode()) {
                if (charSequence.length() == 0) {
                    txt_validity.requestFocus();
                }
            }
        }
        setAddButtonState();
    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (txt_card_number.getText().hashCode() == editable.hashCode()) {
            if (editable.length() == 19) {
                setAddButtonState();
                txt_validity.requestFocus();
            }
        } else if (txt_validity.getText().hashCode() == editable.hashCode()) {
            if (editable.length() == 5) {
                setAddButtonState();
                txt_cvv.requestFocus();
            }
        } else if (txt_cvv.getText().hashCode() == editable.hashCode()) {
            if (editable.length() == 3) {
                setAddButtonState();
            }
        }
    }

    @Override
    public void onShowCard(Card card) {

    }

    @Override
    public void onCardDeleted() {

    }

    @Override
    public void onNext(String message) {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, "Credit Card: "+message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showComplete() {
        setResult(RESULT_OK);
        finish();
        startActivity(new Intent(PaymentCardActivity.this, PaymentPageActivity.class));
    }

    @Override
    public void showProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        mHandler.post(() -> progress_layout.setVisibility(View.GONE));
    }

}
