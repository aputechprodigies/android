package com.atos.fishnet.eWallet.model;

import com.google.gson.annotations.SerializedName;

public class EwalletModel {

    @SerializedName("qr_id")
    private String qr_id;

    @SerializedName("status")
    private String status;

    @SerializedName("balance")
    private String balance;

    @SerializedName("card")
    private String card;

    public String getQr_id() {
        return qr_id;
    }

    public void setQr_id(String qr_id) {
        this.qr_id = qr_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
}
