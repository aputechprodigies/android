package com.atos.fishnet.eWallet.presenter;

import android.content.Context;
import android.util.Log;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.model.Root;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PaymentPresenterImpl implements PaymentContract.Presenter {
    private APIInterface mApiInterface;
    private PaymentContract.View mView;

    @Inject
    public PaymentPresenterImpl(APIInterface mApiInterface, PaymentContract.View view) {
        this.mApiInterface = mApiInterface;
        this.mView = view;
    }

    @Override
    public void getCard() {
        mView.showProgress();
        mApiInterface.getCreditCard(FishnetStorage.getInstance().getCurrentUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // mView.showError(ErrorUtils.getError(e));
                    }

                    @Override
                    public void onNext(Root status) {
                        mView.hideProgress();
                        mView.onShowCard(status.getCard());
                    }
                });
    }

    @Override
    public void deleteCard() {
        mView.showProgress();
        mApiInterface.deleteCreditCard(FishnetStorage.getInstance().getCurrentUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // mView.showError(ErrorUtils.getError(e));
                    }

                    @Override
                    public void onNext(Root status) {
                        mView.hideProgress();
                        mView.onCardDeleted();
                    }
                });
    }

    @Override
    public void uploadCard(String token) {
        mView.showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", FishnetStorage.getInstance().getCurrentUserId());
            jsonObject.put("stripetoken", token);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body =
                RequestBody.create(MediaType.parse("raw"), jsonObject.toString());

        mApiInterface.createCreditCard(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // mView.hideProgress();
                        mView.showError(e.getMessage());
                        Log.e("ErrorCardUPDT",e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Root status) {
                        mView.hideProgress();

                        if (!status.getStatus()) {
                            mView.showError(status.getMessage());

                        } else {
                            FishnetStorage.getInstance().setCreditCardAdded(true);
                            mView.onNext(status.getMessage());
                        }


                    }
                });
    }
}
