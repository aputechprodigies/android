package com.atos.fishnet.eWallet.di.module;

import com.atos.fishnet.di.scope.ActivityScope;
import com.atos.fishnet.eWallet.presenter.PaymentContract;

import dagger.Module;
import dagger.Provides;

@Module
public class PaymentPresenterModule {
    private PaymentContract.View mView;

    public PaymentPresenterModule(PaymentContract.View view){
        this.mView = view;
    }

    @Provides
    @ActivityScope
    PaymentContract.View provideView(){
        return mView;
    }
}
