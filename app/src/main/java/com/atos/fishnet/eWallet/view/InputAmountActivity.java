package com.atos.fishnet.eWallet.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.atos.fishnet.R;
import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.eWallet.presenter.EWalletActivityContract;
import com.atos.fishnet.eWallet.presenter.EWalletPresenterImpl;
import com.atos.fishnet.fishnet.view.BaseActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.abhinay.input.CurrencyEditText;

import static me.abhinay.input.CurrencySymbols.MALAYSIA;

public class InputAmountActivity extends BaseActivity implements EWalletActivityContract.View{

    @BindView(R.id.et_currency)
    CurrencyEditText et_currency;
    @BindView(R.id.txt_balance)
    TextView txt_balance;

    @BindView(R.id.img_back)
    ImageView img_back;

    private EWalletPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_amount);
        ButterKnife.bind(this);
        mPresenter = new EWalletPresenterImpl(this);

        et_currency.setCurrency(MALAYSIA);
        et_currency.setDelimiter(false);
        et_currency.setSpacing(false);
        et_currency.setDecimals(false);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void reload(View view) {

        if (et_currency.getCleanDoubleValue()<=0)
            Toast.makeText(getApplicationContext(), "Please insert the amount", Toast.LENGTH_SHORT).show();
         else
            mPresenter.onTopUpWithCard(FishnetStorage.getInstance().getCurrentUserId(), et_currency.getCleanDoubleValue());
    }

    @Override
    protected void onStart() {
        super.onStart();
        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatCurrency = new DecimalFormatSymbols();

        formatCurrency.setCurrencySymbol("RM ");
        formatCurrency.setMonetaryDecimalSeparator('.');
        formatCurrency.setGroupingSeparator(',');

        df.setDecimalFormatSymbols(formatCurrency);
        txt_balance.setText(df.format(Double.valueOf(FishnetStorage.getInstance().getWalletBalance())));
    }

    public void onBalanceChanged(String balance) {
        FishnetStorage.getInstance().setWalletBalance(balance);
    }

    @Override
    public void onNext(String status) {
        Toast.makeText(this, "" + status, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void showError(String msg) {

    }
}
