package com.atos.fishnet.eWallet.presenter;

import android.content.Context;

import com.atos.fishnet.eWallet.model.Card;

public class PaymentContract {
    public interface View{

        void onShowCard(Card card);

        void onCardDeleted();

        void onNext(String message);

        void showError(String message);

        void showComplete();

        void showProgress();

        void hideProgress();
    }
    interface Presenter{
        void getCard();

        void deleteCard();

        void uploadCard(String token);
    }
}
