package com.atos.fishnet.eWallet.di.module;

import android.content.Context;

import com.atos.fishnet.di.qualifier.ActivityContext;
import com.atos.fishnet.di.scope.ActivityScope;
import com.atos.fishnet.eWallet.view.PaymentCardActivity;
import com.atos.fishnet.eWallet.view.PaymentPageActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class PaymentContextModule {

    private PaymentCardActivity mPaymentCardActivity;
    private PaymentPageActivity mPaymentPageActivity;

    public Context mContext;



    public PaymentContextModule(PaymentCardActivity activity){
        this.mPaymentCardActivity = activity;
    }

    public PaymentContextModule(PaymentPageActivity activity){
        this.mPaymentPageActivity = activity;
    }



    @Provides
    @ActivityScope
    public PaymentCardActivity providePaymentCardActivity(){
        return mPaymentCardActivity;
    }

    @Provides
    @ActivityScope
    public PaymentPageActivity providePaymentPageActivity(){
        return mPaymentPageActivity;
    }


    @Provides
    @ActivityScope
    @ActivityContext
    public Context provideContext(){
        return mContext;
    }
}


