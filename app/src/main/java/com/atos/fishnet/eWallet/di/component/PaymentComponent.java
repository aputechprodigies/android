package com.atos.fishnet.eWallet.di.component;

import android.content.Context;

import com.atos.fishnet.di.component.ApplicationComponent;
import com.atos.fishnet.di.qualifier.ActivityContext;
import com.atos.fishnet.di.scope.ActivityScope;
import com.atos.fishnet.eWallet.di.module.PaymentContextModule;
import com.atos.fishnet.eWallet.di.module.PaymentPresenterModule;
import com.atos.fishnet.eWallet.view.PaymentCardActivity;
import com.atos.fishnet.eWallet.view.PaymentPageActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {PaymentContextModule.class, PaymentPresenterModule.class}, dependencies = ApplicationComponent.class)
public interface PaymentComponent {
    @ActivityContext
    Context getContext();

    void injectPaymentCardActivity(PaymentCardActivity paymentCardActivity);
    void injectPaymentPageActivity(PaymentPageActivity paymentPageActivity);

}
