package com.atos.fishnet.notification.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.atos.fishnet.R;
import com.atos.fishnet.notification.model.Notification;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.CardViewHolder> {

    private Context mContext;
    private List<Notification> mNotification;
    private NotificationAdapter.NotificationClickListener mCallback;

    public NotificationAdapter(Context context, List<Notification> notificationList){
        this.mContext= context;
        this.mNotification = notificationList;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_notification, parent, false);
        return new NotificationAdapter.CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        Notification notification = mNotification.get(position);

        holder.title.setText(notification.getTitle());
        holder.subtitle.setText(notification.getSubtitle());
        if(notification.getType().equalsIgnoreCase("Endangered"))
            holder.img_icon.setBackgroundResource(R.drawable.round_bg_red);
        else if(notification.getType().equalsIgnoreCase("Promotion"))
            holder.img_icon.setBackgroundResource(R.drawable.round_bg_primary_dark);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null) {
                    if(notification.getType().equalsIgnoreCase("Endangered"))
                        mCallback.onItemClick(notification.getId(),"Endangered");
                    else if(notification.getType().equalsIgnoreCase("Promotion"))
                        mCallback.onItemClick(notification.getId(),"Promotion");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNotification.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subtitle)
        TextView subtitle;
        @BindView(R.id.img_icon)
        ImageView img_icon;
        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void setOnNotificationClickListener(final NotificationClickListener listener) {
        mCallback = listener;
    }

    public interface NotificationClickListener {
        void onItemClick(String id, String type);
    }

}