package com.atos.fishnet.notification.presenter;

import android.content.Context;
import android.view.View;

import com.atos.fishnet.database.FishnetStorage;
import com.atos.fishnet.di.module.APIInterface;
import com.atos.fishnet.di.module.RetrofitModule;
import com.atos.fishnet.model.Root;
import com.atos.fishnet.notification.model.Notification;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NotificationPresenterImpl {
    private APIInterface mApiInterface;
    private View mView;
    private Context mContext;

    public NotificationPresenterImpl(Context context, View view){
        this.mView = view;
        this.mContext = context;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    public void getNotificationList(){
        mView.showProgress();
        mApiInterface.getAllNotification().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        mView.onError(e.getMessage());

                    }

                    @Override
                    public void onNext(Root root) {
                        mView.dismissProgress();
                        mView.onNext(root.getNotifications());
                    }
                });
    }

    public void getNotification(String notificationID){
        mView.showProgress();
        JSONObject object = new JSONObject();
        try {
            object.put("notification_id", notificationID);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());

        mApiInterface.getNotification(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Notification>() {
            @Override
            public void onCompleted() {
                mView.showComplete();
            }

            @Override
            public void onError(Throwable e) {
                mView.dismissProgress();
                mView.onError(e.getMessage());
            }

            @Override
            public void onNext(Notification notification) {
                mView.dismissProgress();
                mView.onNextDetails(notification);
            }
        });
    }

    public interface View {

        void onError(String error);

        void showComplete();

        void showProgress();

        void onNext(List<Notification> notificationList);

        void onNextDetails(Notification notification);

        void dismissProgress();

    }
}
