package com.atos.fishnet.notification.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.atos.fishnet.R;
import com.atos.fishnet.barchart.BarChart;
import com.atos.fishnet.barchart.BarChartModel;
import com.atos.fishnet.fishnet.view.MainFragment;
import com.atos.fishnet.notification.model.Notification;
import com.atos.fishnet.notification.presenter.NotificationPresenterImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NotificationFragment extends MainFragment implements NotificationAdapter.NotificationClickListener, SwipeRefreshLayout.OnRefreshListener, NotificationPresenterImpl.View {
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipe_refresh_container;

    Handler mHandler = new Handler(Looper.getMainLooper());

    private NotificationAdapter mAdapter;
    private Unbinder mUnBinder;
    private NotificationPresenterImpl mPresenter;
    public static NotificationFragment getInstance(){
        return new NotificationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        mPresenter = new NotificationPresenterImpl(getContext(),this);
        swipe_refresh_container.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
         mPresenter.getNotificationList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    @Override
    public void onItemClick(String id, String type) {
        Intent intent = new Intent(NotificationFragment.this.getActivity(),NotificationDetailsActivity.class);
        intent.putExtra("type",type);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {
        mHandler.post(() ->
                {
                    if (swipe_refresh_container != null)
                        swipe_refresh_container.setRefreshing(true);
                }
        );
    }

    @Override
    public void onNext(List<Notification> notificationList) {
        mAdapter = new NotificationAdapter(getContext(),notificationList);
        recycler_view.setAdapter(mAdapter);
        mAdapter.setOnNotificationClickListener(this);
    }

    @Override
    public void onNextDetails(Notification notification) {

    }

    @Override
    public void dismissProgress() {
        mHandler.post(() -> {
            if (swipe_refresh_container != null) {
                swipe_refresh_container.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        mPresenter.getNotificationList();
    }
}
