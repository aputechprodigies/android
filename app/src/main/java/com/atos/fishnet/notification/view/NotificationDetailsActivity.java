package com.atos.fishnet.notification.view;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.atos.fishnet.R;
import com.atos.fishnet.fishnet.view.BaseActivity;
import com.atos.fishnet.notification.model.Notification;
import com.atos.fishnet.notification.presenter.NotificationPresenterImpl;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationDetailsActivity extends BaseActivity implements NotificationPresenterImpl.View {

    @BindView(R.id.app_bar_layout)
    AppBarLayout app_bar_layout;
    @BindView(R.id.endangered_container)
    LinearLayout endangered_container;
    @BindView(R.id.promotion_container)
    LinearLayout promotion_container;
    @BindView(R.id.icon_view)
    ImageView icon_view;
    @BindView(R.id.toolbar_left_btn)
    ImageView toolbar_left_btn;

    @BindView(R.id.title_endangered)
    TextView title_endangered;
    @BindView(R.id.subtitle_endangered)
    TextView subtitle_endangered;
    @BindView(R.id.time_endangered)
    TextView time_endangered;
    @BindView(R.id.img_fish)
    ImageView img_fish;
    @BindView(R.id.fish_name)
    TextView fish_name;

    @BindView(R.id.title_promotion)
    TextView title_promotion;
    @BindView(R.id.subtitle_promotion)
    TextView subtitle_promotion;
    @BindView(R.id.time_promotion)
    TextView time_promotion;
    @BindView(R.id.img_promotion)
    ImageView img_promotion;
    @BindView(R.id.txt_data)
    TextView txt_data;

    private Notification notification;
    private String type,id;
    NotificationPresenterImpl notificationPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);
        ButterKnife.bind(this);
        notificationPresenter = new NotificationPresenterImpl(this,this);
        type = getIntent().getExtras().getString("type");
        id = getIntent().getExtras().getString("id");

        toolbar_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        notificationPresenter.getNotification(id);
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void onNext(List<Notification> notificationList) {

    }

    @Override
    public void onNextDetails(Notification notification) {
        if(notification.getType().equalsIgnoreCase("promotion")){
            promotion_container.setVisibility(View.VISIBLE);
            endangered_container.setVisibility(View.GONE);
            app_bar_layout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            icon_view.setImageDrawable(getResources().getDrawable(R.drawable.ic_icons8_promotion));
            updateStatusBarColor("#357DF9");

            title_promotion.setText(notification.getTitle());
            subtitle_promotion.setText(notification.getSubtitle());
            time_promotion.setText(notification.getTime());
            txt_data.setText(notification.getData());
            Glide
                    .with(this)
                    .load(Uri.parse(notification.getImage_file()))
                    .centerCrop()
                    .thumbnail(0.1f)
                    .placeholder(R.drawable.bar_curve_white)
                    .into(img_promotion);
        }else{
            promotion_container.setVisibility(View.GONE);
            endangered_container.setVisibility(View.VISIBLE);
            app_bar_layout.setBackgroundColor(getResources().getColor(R.color.red));
            icon_view.setImageDrawable(getResources().getDrawable(R.drawable.ic_icons8_error));
            updateStatusBarColor("#e74c3c");

            title_endangered.setText(notification.getTitle());
            subtitle_endangered.setText(notification.getSubtitle());
            time_endangered.setText(notification.getTime());
            Glide
                    .with(this)
                    .load(Uri.parse(notification.getUrl()))
                    .centerCrop()
                    .thumbnail(0.1f)
                    .placeholder(R.drawable.bar_curve_white)
                    .into(img_fish);

             fish_name.setText(notification.getFish_name());
        }
    }

    @Override
    public void dismissProgress() {

    }
}
